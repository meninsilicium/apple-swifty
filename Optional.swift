//
//  author:       fabrice truillot de chambrier
//  created:      09.01.2015
//
//  license:      see license.txt
//
//  © 2015-2015, men in silicium sàrl
//


// Iterable

extension Optional : Iterable_ {

	typealias ValueType = T


	func foreach( @noescape function: (T) -> Void ) {
		switch self {
			case .Some( let value ):
				function( value )

			case .None:
				break
		}
	}

	func foreach( @noescape function: (Int, T) -> Void ) {
		switch self {
			case .Some( let value ):
				function( 0, value )

			case .None:
				break
		}
	}

}

/// Optional extension providing the usual monad functions

// MARK: Functor

extension Optional : Functor_ {

	typealias MappedType = Any
	typealias MappedFunctorType = Optional<MappedType>

	// functor map, <^>
	// already defined in Optional
/*
	func map<R>( @noescape transform: (T) -> R ) -> R? {
		switch self {
			case .Some( let value ):
				return transform( value )

			case .None:
				return nil
		}
	}
*/

}

// functor map, <^>
public func <^><T, R>( optional: T?, @noescape transform: (T) -> R ) -> R? {
	return optional.map( transform )
}

// MARK: Pointed

extension Optional : Pointed_ {

	static func pure( value: T ) -> T? {
		return value
	}

}

// MARK: Applicative

extension Optional : Applicative_ {

	typealias ApplicativeType = Optional<(T) -> MappedType>


	// applicative apply, <*>
	static func apply<R>( transform: ((T) -> R)?, value: T? ) -> R? {
		switch (transform, value) {
			case (.Some, .Some):
				return transform!( value! )

			default:
				return nil
		}
	}

}

// applicative apply, <*>
public func <*><T, R>( transform: Optional<(T) -> R>, value: Optional<T> ) -> Optional<R> {
	return Optional.apply( transform, value: value )
}

// MARK: Monad

extension Optional : Monad_ {

	// monad bind, >>*
	func fmap<R>( @noescape transform: (T) -> R? ) -> R? {
		switch self {
			case .Some( let value ):
				return Optional<R>.flatten( self.map( transform ) )

			case .None:
				return nil
		}
	}

	// flatten
	static func flatten( optional: T?? ) -> T? {
		switch optional {
			case .Some( let value ):
				return value

			case .None:
				return nil
		}
	}

}

// monad bind, >>*
public func >>*<T, R>( value: T?, @noescape transform: (T) -> R? ) -> R? {
	return value.fmap( transform )
}

public func *<<<T, R>( @noescape transform: (T) -> R?, value: T? ) -> R? {
	return value.fmap( transform )
}

// MARK: chaining

extension Optional {

	func onSome( @noescape function: (T) -> Void ) -> Optional {
		switch self {
			case .Some( let value ):
				function( value )

			case .None:
				break
		}

		return self
	}

	func onNone( @noescape function: () -> Void ) -> Optional {
		switch self {
			case .Some:
				break

			case .None:
				function()
		}

		return self
	}

}

// MARK: -
// MARK: ImplicitlyUnwrappedOptional

// Swift special treatment of ImplicitlyUnwrappedOptional
// causes a lot of issues and conflicts with the Optional extensions above.
// Also, instance methods are useless.

// TODO: check if this compiles when a new compiler is released
// func foo<T>( optional: Optional<T> ) {}
// func foo<T>( optional: ImplicitlyUnwrappedOptional<T> ) {}

// MARK: Functor

extension ImplicitlyUnwrappedOptional {//: Functor_ {

	typealias MappedType = Any
	typealias MappedFunctorType = ImplicitlyUnwrappedOptional<MappedType>

	// functor map, <^>
	// already defined in ImplicitlyUnwrappedOptional, but...
/*
	func map<R>( @noescape transform: (T) -> R ) -> R! {
		switch self {
			case .Some( let value ):
				return transform( value )

			case .None:
				return nil
		}
	}
*/
}

// functor map, <^>
// Conflicts with Optional.<^>

// MARK: Applicative

extension ImplicitlyUnwrappedOptional {//: Applicative_ {

	typealias ApplicativeType = ImplicitlyUnwrappedOptional<(T) -> MappedType>


	// applicative apply, <*>
	static func apply<R>( transform: ((T) -> R)!, value: T! ) -> R! {
		switch (transform, value) {
			case (.Some, .Some):
				return transform!( value! )

			default:
				return nil
		}
	}

}

// applicative apply, <*>
// Conflicts with Optional.<*>

// MARK: Monad

extension ImplicitlyUnwrappedOptional {//: Monad_ {

	// monad bind, >>*
	func fmap<R>( @noescape transform: (T) -> R! ) -> R! {
		switch self {
			case .Some( let value ):
				return ImplicitlyUnwrappedOptional<R>.flatten( self.map( transform ) )

			case .None:
				return nil
		}
	}

	// flatten
	static func flatten( optional: T!! ) -> T! {
		switch optional {
			case .Some( let value ):
				return value

			case .None:
				return nil
		}
	}

}

// monad bind, >>*
// Conflicts with Optional.>>* and Optional.*<<
