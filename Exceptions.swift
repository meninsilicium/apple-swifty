//
//  author:       fabrice truillot de chambrier
//  created:      03.09.2014
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


import class Foundation.NSException


// attempt
// calls a function, catches any exception and returns a ResultOf
public func attempt<R>( function: () -> R ) -> ResultOf<R> {
	var result: R? = nil
	var error: Error? = nil

	Exceptions.try {
		result = function()
	}
	.catch { (exception) in
		error = Error( name: "Exceptions.attempt.catch", domain: "Exceptions" )
	}

	if let result = result {
		return ResultOf( result )
	}
	else if let error = error {
		return ResultOf<R>( error: error )
	}
	else {
		return ResultOf<R>( error: nil )
	}
}

// submodule Exceptions
public enum Exceptions : Submodule {


public static func try( function: () -> Void ) -> Catch {
	return Try.try( function )
}

public static func throw( # message: String, reason: String ) {
	return Try.throw( message: message, reason: reason )
}


public struct Try {

	public static func try( function: () -> Void ) -> Catch {
		let exception = ExceptionsHelper.try( function )

		return Catch( exception )
	}

	public static func throw( # message: String, reason: String ) {
		ExceptionsHelper.throw( message, reason: reason )
	}

}


public struct Catch {

	public let exception: NSException?

	public init( _ exception: NSException? ) {
		self.exception = exception
	}

	public func catch( function: (NSException) -> Void ) -> Finally {
		if let exception = self.exception {
			function( exception )
		}

		return Finally()
	}

	public func finally( function: () -> Void ) -> Void {
		function()
	}
	
}


public struct Finally {

	public init() {
	}

	public func finally( function: () -> Void ) -> Void {
		function()
	}
	
}


}
