//
//  author:       fabrice truillot de chambrier
//  created:      19.01.2015
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


// References:
// https://www.haskell.org/haskellwiki/Typeclassopedia
// https://www.haskell.org/wikiupload/8/85/TMR-Issue13.pdf
// https://themonadreader.wordpress.com/


// apply

infix operator <*> { associativity left precedence 150 }


// MARK: Applicative

// ApplicativeSpecifics

public protocol ApplicativeSpecifics {

	typealias ValueType
	typealias MappedType
	typealias MappedFunctorType = Generic1<MappedType>
	typealias ApplicativeType = Generic1<(ValueType) -> MappedType>


	static func apply( transform: ApplicativeType, value: Self ) -> MappedFunctorType

	func <*>( transform: ApplicativeType, value: Self ) -> MappedFunctorType
	
}

// Applicative

public protocol Applicative : Pointed, ApplicativeSpecifics {}

public func <*><T: Applicative>( transform: T.ApplicativeType, value: T ) -> T.MappedFunctorType {
	return T.apply( transform, value: value )
}


// MARK: -
// MARK: internal Applicative

protocol ApplicativeSpecifics_ {

	typealias ValueType
	typealias MappedType
	typealias MappedFunctorType = Generic1<MappedType>
	typealias ApplicativeType = Generic1<(ValueType) -> MappedType>


	static func apply( transform: ApplicativeType, value: Self ) -> MappedFunctorType

	func <*>( transform: ApplicativeType, value: Self ) -> MappedFunctorType
	
}

// Applicative

protocol Applicative_ : Pointed_, ApplicativeSpecifics_ {}

func <*><T: Applicative_>( transform: T.ApplicativeType, value: T ) -> T.MappedFunctorType {
	return T.apply( transform, value: value )
}
