//
//  author:       fabrice truillot de chambrier
//  created:      19.01.2015
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


// References:
// https://www.haskell.org/haskellwiki/Typeclassopedia
// https://www.haskell.org/wikiupload/8/85/TMR-Issue13.pdf
// https://themonadreader.wordpress.com/


// >>*, fmap / bind / Haskell's >>=

infix operator >>* { associativity left precedence 150 }

// *<<, reversed bind / Haskell's =<<

infix operator *<< { associativity left precedence 150 }


// MARK: Monad

// MonadSpecifics

public protocol MonadSpecifics {

	typealias ValueType

	typealias MappedType
	typealias MappedFunctorType = Generic1<MappedType>


	func fmap( @noescape transform: (ValueType) -> MappedFunctorType ) -> MappedFunctorType // Haskell's bind, Scala's flatMap

	func >>*( value: Self, @noescape transform: (ValueType) -> MappedFunctorType ) -> MappedFunctorType
	func *<<( @noescape transform: (ValueType) -> MappedFunctorType, value: Self ) -> MappedFunctorType

}

public func >>*<T: MonadSpecifics>( value: T, @noescape transform: (T.ValueType) -> T.MappedFunctorType ) -> T.MappedFunctorType {
	return value.fmap( transform )
}

public func *<<<T: MonadSpecifics>( @noescape transform: (T.ValueType) -> T.MappedFunctorType, value: T ) -> T.MappedFunctorType {
	return value.fmap( transform )
}

// Monad

public protocol Monad : Applicative, MonadSpecifics {}

public func >>*<T: Monad>( value: T, @noescape transform: (T.ValueType) -> T.MappedFunctorType ) -> T.MappedFunctorType {
	return value.fmap( transform )
}

public func *<<<T: Monad>( @noescape transform: (T.ValueType) -> T.MappedFunctorType, value: T ) -> T.MappedFunctorType {
	return value.fmap( transform )
}


// MARK: -
// MARK: internal

// MonadSpecifics

protocol MonadSpecifics_ {

	typealias ValueType
	typealias MappedType
	typealias MappedFunctorType = Generic1<MappedType>


	func fmap( @noescape transform: (ValueType) -> MappedFunctorType ) -> MappedFunctorType // Haskell's bind, Scala's flatMap

	func >>*( value: Self, @noescape transform: (ValueType) -> MappedFunctorType ) -> MappedFunctorType
	func *<<( @noescape transform: (ValueType) -> MappedFunctorType, value: Self ) -> MappedFunctorType

}

func >>*<T: MonadSpecifics_>( value: T, @noescape transform: (T.ValueType) -> T.MappedFunctorType ) -> T.MappedFunctorType {
	return value.fmap( transform )
}

func *<<<T: MonadSpecifics_>( @noescape transform: (T.ValueType) -> T.MappedFunctorType, value: T ) -> T.MappedFunctorType {
	return value.fmap( transform )
}

// Monad

protocol Monad_ : Applicative_, MonadSpecifics_ {}

func >>*<T: Monad_>( value: T, @noescape transform: (T.ValueType) -> T.MappedFunctorType ) -> T.MappedFunctorType {
	return value.fmap( transform )
}

func *<<<T: Monad_>( @noescape transform: (T.ValueType) -> T.MappedFunctorType, value: T ) -> T.MappedFunctorType {
	return value.fmap( transform )
}
