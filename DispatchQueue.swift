//
//  author:       fabrice truillot de chambrier
//  created:      23.03.2015
//
//  license:      see license.txt
//
//  © 2015-2015, men in silicium sàrl
//


import Foundation
import Dispatch


public class DispatchQueue {

	public static var MainQueue = DispatchQueue()

	public static var InteractiveQueue = DispatchQueue( qos: QOS_CLASS_USER_INTERACTIVE )
	public static var InitiatedQueue = DispatchQueue( qos: QOS_CLASS_USER_INITIATED )
	public static var UtilityQueue = DispatchQueue( qos: QOS_CLASS_UTILITY )
	public static var BackgroundQueue = DispatchQueue( qos: QOS_CLASS_BACKGROUND )


	// MARK: initialization

	public init() {
		self.queue = dispatch_get_main_queue()
	}

	public init( queue: DispatchQueue ) {
		self.queue = queue.queue
	}

	public init( label: String, concurrent: Bool, target: DispatchQueue? = nil ) {
		self.queue = dispatch_queue_create( label, concurrent ? DISPATCH_QUEUE_CONCURRENT : DISPATCH_QUEUE_SERIAL )
		target <^> {
			dispatch_set_target_queue( self.queue, $0.queue )
		}
	}

	private init( qos: qos_class_t ) {
		self.queue = dispatch_get_global_queue( Int( qos.value ), 0 )
	}

	deinit {
		self.context = nil
	}

	public func setTargetQueue( queue: DispatchQueue ) {
		dispatch_set_target_queue( self.queue, queue.queue )
	}

	// MARK: attributes

	public var label: String {
		let label = dispatch_queue_get_label( self.queue )
		return String.fromCString( label )!
	}

	public var context: AnyObject? {
		get {
			let pointer = dispatch_queue_get_specific( self.queue, DispatchQueue.contextKey )
			if pointer == nil { return nil }

			// retrieve the value, without consuming a reference
			let unmanaged = Unmanaged<AnyObject>.fromOpaque( COpaquePointer( pointer ) )
			let context: AnyObject = unmanaged.takeUnretainedValue()

			return context
		}

		set( value ) {
			value.onSome { (value) -> Void in
				// releasing the previous value
				self.context = nil

				// create a pointer to the value, retained
				let unmanaged = Unmanaged<AnyObject>.passRetained( value )
				let pointer = UnsafeMutablePointer<Void>( unmanaged.toOpaque() )

				dispatch_queue_set_specific( self.queue, DispatchQueue.contextKey, pointer, nil )
			}
			.onNone { () -> Void in
				let pointer = dispatch_queue_get_specific( self.queue, DispatchQueue.contextKey )
				if pointer == nil { return }

				//dispatch_set_context( self.queue, nil )
				dispatch_queue_set_specific( self.queue, DispatchQueue.contextKey, nil, nil )

				// releasing
				let unmanaged = Unmanaged<AnyObject>.fromOpaque( COpaquePointer( pointer ) )
				unmanaged.release()
			}
		}
	}

	// MARK: resume / suspend

	public func resume() {
		dispatch_resume( self.queue )
	}

	public func suspend() {
		dispatch_suspend( self.queue )
	}

	// MARK: dispatch

	public func async( function: () -> Void ) {
		dispatch_async( self.queue, function )
	}

	public func async( barrier: Bool, function: () -> Void ) {
		if barrier {
			dispatch_barrier_async( self.queue, function )
		}
		else {
			dispatch_async( self.queue, function )
		}
	}

	public func sync( function: () -> Void ) {
		dispatch_sync( self.queue, function )
	}

	public func sync( barrier: Bool, function: () -> Void ) {
		if barrier {
			dispatch_barrier_sync( self.queue, function )
		}
		else {
			dispatch_sync( self.queue, function )
		}
	}

	public func after( time: DispatchTime, function: () -> Void ) {
		dispatch_after( time.time, self.queue, function )
	}


	// GCD queue
	private var queue: dispatch_queue_t

	// self.context needs a key, and Swift doesn't help much here
	private static let contextKeyValue: NSString = "context key"
	private static let contextKey = UnsafePointer<Void>( Unmanaged<NSString>.passUnretained( DispatchQueue.contextKeyValue ).toOpaque() )

}


// MARK: DispatchQueue : Equatable

extension DispatchQueue : Equatable {}

public func ==( lhs: DispatchQueue, rhs: DispatchQueue ) -> Bool {
	return lhs.queue === rhs.queue
}
