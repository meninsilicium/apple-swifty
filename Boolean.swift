//
//  author:       fabrice truillot de chambrier
//  created:      09.01.2015
//
//  license:      see license.txt
//
//  © 2015-2015, men in silicium sàrl
//


/// Why is Bool a struct in Swift? It should be an enum, so here is mine.

public enum Boolean {

	case False
	case True
	
}

let True = Boolean( true )
let False = Boolean( false )

extension Boolean {

	public init() {
		self = .False
	}

	public init( _ value: Boolean ) {
		self = value
	}

	public init( _ value: Bool ) {
		self = value ? .True : .False
	}

	public init( _ value: BooleanType ) {
		self = value ? .True : .False
	}

}

extension Boolean : BooleanLiteralConvertible {

	public init( booleanLiteral value: BooleanLiteralType ) {
		self = value ? .True : .False
	}

}

extension Boolean : BooleanType {

	public var boolValue: Bool {
		switch self {
			case .False: return false
			case .True:  return true
		}
	}

}

extension Boolean : Equatable {}

public func ==( lhs: Boolean, rhs: Boolean ) -> Bool {
	switch (lhs, rhs) {
		case (.False, .False), (.True, .True):  return true
		case (.False, .True),  (.True, .False): return false
	}
}

public func ==( lhs: Boolean, rhs: Boolean ) -> Boolean {
	switch (lhs, rhs) {
		case (.False, .False), (.True, .True):  return true
		case (.False, .True),  (.True, .False): return false
	}
}

public prefix func !( value: Boolean ) -> Boolean {
	switch value {
		case .False: return true
		case .True:  return false
	}
}

public prefix func ~( value: Boolean ) -> Boolean {
	return !value
}

public func &( lhs: Boolean, rhs: Boolean ) -> Boolean {
	switch lhs {
		case .False: return .False
		case .True:  return rhs
	}
}

public func &&( lhs: Boolean, rhs: Boolean ) -> Boolean {
	switch lhs {
		case .False: return .False
		case .True:  return rhs
	}
}

public func &&( lhs: Boolean, rhs: Bool ) -> Boolean {
	switch lhs {
		case .False: return .False
		case .True:  return Boolean( rhs )
	}
}

public func &&( lhs: Bool, rhs: Boolean ) -> Boolean {
	return lhs ? rhs : .False
}

public func !&&( lhs: Boolean, rhs: Boolean ) -> Boolean {
	return !(lhs && rhs)
}

public func |( lhs: Boolean, rhs: Boolean ) -> Boolean {
	switch lhs {
		case .False: return rhs
		case .True:  return true
	}
}

public func ||( lhs: Boolean, rhs: Boolean ) -> Boolean {
	switch lhs {
		case .False: return rhs
		case .True:  return true
	}
}

public func !||( lhs: Boolean, rhs: Boolean ) -> Boolean {
	return !(lhs || rhs)
}

public func ^( lhs: Boolean, rhs: Boolean ) -> Boolean {
	switch (lhs, rhs) {
		case (.False, .False), (.True, .True):  return false
		case (.False, .True),  (.True, .False): return true
	}
}

public func ^^( lhs: Boolean, rhs: Boolean ) -> Boolean {
	switch (lhs, rhs) {
		case (.False, .False), (.True, .True):  return false
		case (.False, .True),  (.True, .False): return true
	}
}

public func !^^( lhs: Boolean, rhs: Boolean ) -> Boolean {
	return !(lhs ^^ rhs)
}

// MARK: chaining

extension Boolean {

	public func onTrue( @noescape function: () -> Void ) -> Boolean {
		switch self {
			case .False: break
			case .True:  function()
		}

		return self
	}

	public func onFalse( @noescape function: () -> Void ) -> Boolean {
		switch self {
			case .False: function()
			case .True:  break
		}

		return self
	}

}


// MARK: Bool

extension Bool {

	public func onTrue( @noescape function: () -> Void ) -> Bool {
		if self {
			function()
		}

		return self
	}

	public func onFalse( @noescape function: () -> Void ) -> Bool {
		if self {
			function()
		}

		return self
	}

}
