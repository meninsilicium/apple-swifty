//
//  author:       fabrice truillot de chambrier
//  created:      30.07.2014
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


import Darwin
import Foundation


// Snippets:
// let marker: JSONValue = [ "marker" : [ "location" : [ "latitude" : 46.2050295, "longitude" : 6.1440885 ], "color" : "#ffcc00" ] ]
// let color = marker[ "marker", "color" ]?.string
// let path: JSONPath = [ "marker", "location" ]
// let location = marker[ path ]


// MARK: struct JSON

public struct JSON {

	public let json: JSONValue

	public init( json: JSONValue ) {
		if json.isCollection {
			self.json = json
		}
		else {
			self.json = JSONValue( invalid: nil )
		}
	}

}

// MARK: enum JSONValue

public enum JSONValue {

	// Tree
	case JSONDictionary( [String: JSONValue] )
	case JSONArray( [JSONValue] )

	// Data
	case JSONString( String )
	case JSONNumber( NSNumber )
	case JSONDouble( Double )
	case JSONInteger( Int )
	case JSONBool( Bool )
	case JSONNull

	// Oops
	case Invalid( AnyObject? )

}

// MARK: conforming types

public protocol JSONValueType {}

extension Dictionary : JSONValueType {}
extension NSDictionary : JSONValueType {}

extension Array : JSONValueType {}
extension NSArray : JSONValueType {}

extension String : JSONValueType {}
extension Double : JSONValueType {}
extension Int : JSONValueType {}
extension UInt : JSONValueType {}
extension NSNumber : JSONValueType {}

extension Bool : JSONValueType {}
//extension Boolean : JSONValueType {}

extension NSNull : JSONValueType {}


// MARK: - Decode

extension JSON {

	public typealias JSONResult = ResultOf<JSONValue>


	// decode( NSData )
	public static func decode( data: NSData ) -> JSONResult {
		var error: NSError? = nil
		let object: AnyObject? = NSJSONSerialization.JSONObjectWithData( data, options: NSJSONReadingOptions.MutableContainers, error: &error )

		if error != nil {
			println( error )

			return JSONResult( error: Error( error: error! ) )
		}

		if let object = object as? NSObject {
			var value = JSONValue( any: object )

			return JSONResult( value: value )
		}

		return JSONResult( error: Error( name: "JSON.decode failed", domain: "JSON" ) )
	}

	// decode( String )
	public static func decode( string: String ) -> JSONResult {
		if let data = string.dataUsingEncoding( NSUTF8StringEncoding ) {
			return self.decode( data )
		}

		return JSONResult( error: Error( name: "JSON.decode failed", domain: "JSON" ) )
	}
	
}

// MARK: -
// MARK: initialization with native values

extension JSONValue {

	init() {
		self = .JSONNull
	}

	// MARK: Jack of all trades init
	private init( any value: Any ) {
		let swift = reflect( value ).disposition != MirrorDisposition.ObjCObject
		if swift {
			switch value {
				case let value as JSONValue:
					self.init( value )

				//case let dictionary as [String: Any]:
				// this pattern doesn't match anything, not even a [String: Any] - 2015.02.01

				case let dictionary as NSDictionary:
					self.init( dictionary: dictionary )

				//case let value as [Any]:
				// this pattern doesn't match anything, not even a [Any] - 2015.02.01

				case let value as NSArray:
					self.init( array: value )

				case let value as String:
					self.init( value )

				case let value as Double:
					self.init( value )

				case let value as Int:
					self.init( value )

				case let value as UInt:
					self.init( value )

				case let value as Bool:
					self.init( value )

				case let value as Boolean:
					self.init( value )

				case let value as NSNumber:
					self.init( value )

				case let value as NSNull:
					self.init()

				default:
					self.init( invalid: Box<Any>( value ) )
					println( "init( any: \(value) ) with an unsupported value" )
			}
		}
		else {
			switch value {
				case let dictionary as NSDictionary:
					self.init( dictionary: dictionary )

				case let value as NSArray:
					self.init( array: value )

				case let value as String:
					self.init( value )

				case let value as NSNumber:
					self.init( value )

				case let value as NSNull:
					self.init()

				default:
					self.init( invalid: Box<Any>( value ) )
					println( "init( any: \(value) ) with an unsupported value" )
			}
		}
	}

	public init( any value: JSONValueType ) {
		let swift = reflect( value ).disposition != MirrorDisposition.ObjCObject
		if swift {
			switch value {
				case let dictionary as NSDictionary:
					self.init( dictionary: dictionary )

				case let value as NSArray:
					self.init( array: value )

				case let value as String:
					self.init( value )

				case let value as Double:
					self.init( value )

				case let value as Int:
					self.init( value )

				case let value as UInt:
					self.init( value )

				case let value as Bool:
					self.init( value )

				case let value as NSNumber:
					self.init( value )

				case let value as NSNull:
					self.init()

				default:
					self.init( invalid: Box<Any>( value ) )
					println( "init( any: \(value) ) with an unsupported value" )
			}
		}
		else {
			switch value {
				case let dictionary as NSDictionary:
					self.init( dictionary: dictionary )

				case let value as NSArray:
					self.init( array: value )

				case let value as String:
					self.init( value )

				case let value as NSNumber:
					self.init( value )

				case let value as NSNull:
					self.init()

				default:
					self.init( invalid: Box<Any>( value ) )
					println( "init( any: \(value) ) with an unsupported value" )
			}
		}
	}

	public init( nsobject value: NSObject ) {
		switch value {
			case let dictionary as NSDictionary:
				self.init( dictionary: dictionary )

			case let value as NSArray:
				self.init( array: value )

			case let value as String:
				self.init( value )

			case let value as NSNumber:
				self.init( value )

			case let value as NSNull:
				self.init()

			default:
				self.init( invalid: Box<Any>( value ) )
				println( "init( any: \(value) ) with an unsupported value" )
		}
	}

	// Copy

	public init( _ value: JSONValue ) {
		self = value
	}

	// Dictionary

	public init( _ values: [String: JSONValue] ) {
		self = .JSONDictionary( values )
	}

	public init( _ values: [String: JSONValueType] ) {
		var dictionary = [String: JSONValue]()
		for (key, value) in values {
			println( "key: \(key) -> value: \(value)" )
			dictionary[ key ] = JSONValue( any: value )
		}

		self = .JSONDictionary( dictionary )
	}

	init( dictionary: NSDictionary ) {
		var values = [String: JSONValue]()
		for (key, value) in dictionary {
			if let key = key as? String {
				if let value = value as? NSObject {
					values[ key ] = JSONValue( any: value )
				}
			}
		}

		self = .JSONDictionary( values )
	}

	// Array

	public init( _ values: [JSONValue] ) {
		self = .JSONArray( values )
	}

	public init( _ values: [JSONValueType] ) {
		var array = values.map { JSONValue( any: $0 ) }

		self = .JSONArray( array )
	}

	private init( _ values: NSArray ) {
		var array = [JSONValue]()
		for value in values {
			if let object = value as? NSObject {
				array.append( JSONValue( any: object ) )
			}
		}

		self = .JSONArray( array )
	}

	private init( array: NSArray ) {
		var values = [JSONValue]()
		for value in array {
			if let object = value as? NSObject {
				values.append( JSONValue( any: object ) )
			}
		}

		self = .JSONArray( values )
	}

	// String

	public init( _ value: String ) {
		self = .JSONString( value )
	}

	// Number

	public init( _ value: Double ) {
		self = .JSONDouble( value )
	}

	public init( _ value: Int ) {
		self = .JSONInteger( value )
	}

	public init( _ value: Int64 ) {
		self = .JSONInteger( numericCast( value ) )
	}

	public init<T: SignedIntegerType>( _ value: T ) {
		self = .JSONInteger( numericCast( value ) )
	}

	public init( _ value: UInt ) {
		self = .JSONNumber( NSNumber( unsignedLongLong: numericCast( value ) ) )
	}

	public init( _ value: UInt64 ) {
		self = .JSONNumber( NSNumber( unsignedLongLong: value ) )
	}

	public init<T: UnsignedIntegerType>( _ value: T ) {
		self = .JSONNumber( NSNumber( unsignedLongLong: numericCast( value ) ) )
	}

	public init( _ value: NSNumber ) {
		let type = value.objCType
		// Bool
		if (type == NSNumber( bool: true ).objCType) ||
			(type == NSNumber( bool: false ).objCType ) {
				self = .JSONBool( value.boolValue )
		}
		// Double
		else if (type == NSNumber( double: 0.0 ).objCType ) ||
			(type == NSNumber( float: 0.0 ).objCType ) {
				self = .JSONDouble( value.doubleValue )
		}
		// Integer
		else {
			self = .JSONInteger( value.integerValue )
		}
	}

	// Boolean

	public init( _ value: Bool ) {
		self = .JSONBool( value )
	}

	public init( _ value: Boolean ) {
		self = .JSONBool( value.boolValue )
	}

	public init( _ value: BooleanType ) {
		self = .JSONBool( value.boolValue )
	}

	// Invalid

	public init( invalid: AnyObject? ) {
		self = .Invalid( invalid )
		println( "init( invalid: \(invalid) )" )
	}

}

// MARK: initialization with literals

extension JSONValue : DictionaryLiteralConvertible {

	public typealias Key = String
	public typealias Value = JSONValueType

	public init( dictionaryLiteral elements: (String, JSONValueType)... ) {
		// assemble a dictionary
		var dictionary = [String: JSONValueType]()
		for (key, value) in elements {
			dictionary[ key ] = value
		}

		self.init( dictionary )
	}

}

extension JSONValue : ArrayLiteralConvertible {

	public typealias Element = JSONValueType

	public init( arrayLiteral elements: JSONValueType... ) {
		var array = [JSONValueType]()
		array.extend( elements )

		self.init( array )
	}

}

extension JSONValue : StringLiteralConvertible {

	public init( stringLiteral value: String ) {
		self.init( value )
	}

	public init( extendedGraphemeClusterLiteral value: StaticString ) {
		self.init( value.stringValue )
	}

	public init( unicodeScalarLiteral value: Character ) {
		self.init( String( value ) )
	}

}

extension JSONValue : FloatLiteralConvertible {

	public init( floatLiteral value: Double ) {
		self.init( value )
	}

}

extension JSONValue : IntegerLiteralConvertible {

	public init( integerLiteral value: Int ) {
		self.init( value )
	}

}

extension JSONValue : BooleanLiteralConvertible {

	public init( booleanLiteral value: Bool ) {
		self.init( value )
	}

}

extension JSONValue : NilLiteralConvertible {

	public init( nilLiteral: Void ) {
		self.init()
	}

}

// MARK: -

// MARK: Constants

extension JSON {

	public static let null: JSONValue = JSONValue( nil )

}

extension JSONValue {

	public static let null: JSONValue = JSONValue( nil )

}

// MARK: Accessors

extension JSONValue {

	public func paths() -> [JSONPath] {
		// TODO: return a lazy view instead of an array
		switch self {
			case .JSONDictionary( let dictionary ):
				var paths = [JSONPath]()
				for (key, value) in dictionary {
					let path = JSONPath( key )
					let subpaths = value.paths().filter { $0.count > 0 }.map { (var subpath) -> JSONPath in
						var rpath = path
						rpath.extend( subpath )

						return rpath
					}
					paths.append( path )
					paths.extend( subpaths )
				}
				return paths

			case .JSONArray( let array ):
				var paths = [JSONPath]()
				for index in 0 ..< array.count {
					let path = JSONPath( index )
					let value = array[ index ]
					let subpaths = value.paths().filter { $0.count > 0 }.map { (subpath) -> JSONPath in
						var rpath = path
						rpath.extend( subpath )

						return rpath
					}
					paths.append( path )
					paths.extend( subpaths )
				}
				return paths

			case .JSONString, .JSONNumber, .JSONDouble, .JSONInteger, .JSONBool, .JSONNull, .Invalid:
				return []
		}
	}

	// Flatten the json and returns an array of tuples (path, value)
	public func tuples() -> [(JSONPath, JSONValue)] {
		let paths = self.paths()
		let array = paths.map { (path) -> (JSONPath, JSONValue?) in
				return (path, self[ path ])
			}.filter { (_, value) -> Bool in
				return value != nil
			}.map { (path, value) -> (JSONPath, JSONValue) in
				return (path, value!)
			}

		return array
	}

	public func tuples( path: JSONPath ) -> [(JSONPath, JSONValue)] {
		if let root = self[ path ] {
			return root.tuples().map { (subpath, value) -> (JSONPath, JSONValue) in
				var rpath = path
				rpath.extend( subpath )

				return (rpath, value)
			}
		}
		else {
			return []
		}
	}

	// Returns true when an associated value exists, false for an invalid entry
	private var hasValue: Bool {
		switch self {
			case .JSONDictionary, .JSONArray, .JSONString, .JSONNumber, .JSONDouble, .JSONInteger, .JSONBool, .JSONNull:
				return true

			case .Invalid:
				return false
		}
	}

	// a dictionary or an array?
	public var isCollection: Bool {
		switch self {
			case .JSONDictionary, .JSONArray:
				return true

			case .JSONString, .JSONNumber, .JSONDouble, .JSONInteger, .JSONBool:
				return false

			case .JSONNull:
				return false

			case .Invalid:
				return false
		}
	}

	public var count: Int {
		switch self {
			case .JSONDictionary( let dictionary ):
				return dictionary.count

			case .JSONArray( let array ):
				return array.count

			case .JSONString, .JSONNumber, .JSONDouble, .JSONInteger, .JSONBool:
				return 1

			case .JSONNull:
				return 0

			case .Invalid:
				return 0
		}
	}

	public var dictionary: [String: JSONValue]? {
		switch self {
			case .JSONDictionary( let dictionary ):
				return dictionary

			default:
				return nil
		}
	}

	public var array: [JSONValue]? {
		switch self {
			case .JSONArray( let array ):
				return array

			default:
				return nil
		}
	}

	// a string, number or bool?
	public var isSingle: Bool {
		switch self {
			case .JSONDictionary, .JSONArray:
				return false

			case .JSONString, .JSONNumber, .JSONDouble, .JSONInteger, .JSONBool:
				return true

			case .JSONNull:
				return false

			case .Invalid:
				return false
		}
	}

	public var string: String? {
		switch self {
			case .JSONString( let string ):
				return string

			default:
				return nil
		}
	}

	public var number: NSNumber? {
		switch self {
			case .JSONNumber( let number ):
				return number

			case .JSONDouble( let double ):
				return NSNumber( double: double )

			case .JSONInteger( let integer ):
				return NSNumber( integer: integer )

			case .JSONBool( let bool ):
				return NSNumber( bool: bool )

			default:
				return nil
		}
	}

	public var double: Double? {
		switch self {
			case .JSONNumber( let number ):
				return number.doubleValue

			case .JSONDouble( let double ):
				return double

			case .JSONInteger( let integer ):
				return Double( integer )

			case .JSONBool( let bool ):
				return bool ? 1.0 : 0.0

			default:
				return nil
		}
	}

	public var integer: Int? {
		switch self {
			case .JSONNumber( let number ):
				return number.integerValue

			case .JSONDouble( let double ):
				return Int( round( double ) )

			case .JSONInteger( let integer ):
				return integer

			case .JSONBool( let bool ):
				return bool ? 1 : 0

			default:
				return nil
		}
	}

	public var bool: Bool? {
		switch self {
			case .JSONBool( let bool ):
				return bool

			default:
				return nil
		}
	}

	// null?
	public var isNull: Bool {
		switch self {
			case .JSONDictionary, .JSONArray, .JSONString, .JSONNumber, .JSONDouble, .JSONInteger, .JSONBool:
				return false

			case .JSONNull:
				return true

			case .Invalid:
				return false
		}
	}

	public var null: NSNull? {
		switch self {
			case .JSONNull:
				return NSNull()

			default:
				return nil
		}
	}

	private func read<T>() -> T? {
		return self.value as? T
	}

	private var value: JSONValueType? {
		switch self {
			case .JSONDictionary( let dictionary ):
				return dictionary

			case .JSONArray( let array ):
				return array

			case .JSONString( let string ):
				return string

			case .JSONNumber( let number ):
				return number

			case .JSONDouble( let double ):
				return double

			case .JSONInteger( let integer ):
				return integer

			case .JSONBool( let bool ):
				return bool

			case .JSONNull:
				return NSNull()

			case .Invalid:
				return nil
		}
	}

	private var date: NSDate? {
		switch self {
			case .JSONString( let string ):
				//let date = string.dateJSON()
				let date: NSDate? = nil
				if date != nil {
					return date
				}
				else {
					return nil
				}

			default:
				return nil
		}
	}

}

// MARK: subscripts

extension JSONValue {

	public subscript( component: JSONPathComponentType ) -> JSONValue? {
		return self[ JSONPath( component ) ]
	}

	// subscript with 2+ components
	public subscript( component1: JSONPathComponentType, component2: JSONPathComponentType, components: JSONPathComponentType... ) -> JSONValue? {
		var array = [JSONPathComponentType]()
		array.append( component1 )
		array.append( component2 )
		array.extend( components )

		return self[ JSONPath( array ) ]
	}

	/// subscript with a JSONPath
	public subscript( path: JSONPath ) -> JSONValue? {
		get {
			if let component = path.first {
				if let value = self[ component ] {
					if path.count <= 1 {
						return value
					}
					else {
						return value[ path.tail ]
					}
				}
			}

			return nil
		}
	}

	/// subscript with a JSONPathComponent
	public subscript( component: JSONPathComponent ) -> JSONValue? {
		switch (self, component) {
			case (.JSONDictionary( let dictionary ), .Key( let key ) ):
				return dictionary[ key ]

			case (.JSONArray( let array ), .Index( let index ) ):
				return array[ index ]

			default:
				return nil
		}
	}

	public func getLeafPath( path: JSONPath ) -> JSONPath {
		// naive implementation
		var rpath = path
		while !rpath.isEmpty {
			if let value = self[ rpath ] {
				break
			}

			rpath = rpath.dropLast()
		}

		return rpath
	}

	public func getLeafValue( path: JSONPath ) -> JSONValue {
		let leafpath = self.getLeafPath( path )

		if !leafpath.isEmpty {
			if let value = self[ leafpath ] {
				return value
			}
		}

		return self
	}

	public func getLeaf( path: JSONPath ) -> (JSONPath, JSONValue) {
		// naive implementation
		var rpath = path
		while !rpath.isEmpty {
			if let value = self[ rpath ] {
				return (rpath, value)
			}

			rpath = rpath.dropLast()
		}

		return ([], self)
	}

	public func isSetable( path: JSONPath ) -> Bool {
		if path.isEmpty { return true }

		let (leafpath, leafvalue) = self.getLeaf( path )
		assert( leafpath.count <= path.count )

		if leafpath.isEmpty { return true }
		if leafpath.count == path.count { return true }

		let tailpath = path.drop( leafpath.count )
		assert( !tailpath.isEmpty )
		let tailcomponent = tailpath[0]

		switch (tailcomponent, leafvalue) {
			case (.Key( let key ), .JSONDictionary( let dictionary )):
				return true

			case (.Index( let index ), .JSONArray( let array )):
				return index < array.count

			default:
				return false
		}
	}

	public mutating func set( value: JSONValue ) {
		self = value
	}

}

// MARK: array operations

extension JSONValue {

	public mutating func append( value: JSONValue ) {
		switch self {
			case .JSONArray( var array ):
				array.append( value )
				self = .JSONArray( array )

			default:
				break
		}
	}

	public mutating func extend<S : SequenceType where S.Generator.Element == JSONValue>( elements: S ) {
		switch self {
			case .JSONArray( var array ):
				array.extend( elements )
				self = .JSONArray( array )

			default:
				break
		}
	}

}

// MARK: set operations

extension JSONValue {

	// JSONValue composition
	public static func compose( lhs: JSONValue, _ rhs: JSONValue ) -> JSONValue {
		switch (lhs, rhs) {
			// Dictionaries
			case (.JSONDictionary( let ldictionary ), .JSONDictionary( var rdictionary )):
				var dictionary: Dictionary = [String: JSONValue]()
				for (let key, let lvalue) in ldictionary {
					if let rvalue = rdictionary[ key ] {
						dictionary[ key ] = JSONValue.compose( lvalue, rvalue )
						rdictionary[ key ] = nil
					}
					else {
						dictionary[ key ] = lvalue
					}
				}
				for (let key, let rvalue) in rdictionary {
					dictionary[ key ] = rvalue
				}

				return JSONValue( dictionary )

			// Arrays
			case (.JSONArray( let larray ), .JSONArray( let rarray )):
				var values = [JSONValue]()

				let mincount = min( larray.count, rarray.count )
				for index in 0 ..< mincount {
					let lvalue = larray[ index ]
					let rvalue = rarray[ index ]
					let value = JSONValue.compose( lvalue, rvalue )
					values.append( value )
				}

				let maxcount = max( larray.count, rarray.count )
				if mincount == maxcount {
					return JSONValue( values )
				}

				let maxarray = larray.count >= rarray.count ? larray : rarray
				values.extend( maxarray[ mincount ..< maxcount ] )

				return JSONValue( values )

			case (.JSONNull, _):
				return rhs

			case (_, .JSONNull):
				return lhs

			case (.Invalid, _):
				return rhs

			case (_, .Invalid):
				return lhs

			default:
				return lhs
		}
	}

	public static func intersection( lhs: JSONValue, _ rhs: JSONValue ) -> JSONValue? {
		switch (lhs, rhs) {
			// Dictionaries
			case (.JSONDictionary( let ldictionary ), .JSONDictionary( let rdictionary )):
				var dictionary = [String: JSONValue]()
				for (let key, let lvalue) in ldictionary {
					if let rvalue = rdictionary[ key ] {
						if let ivalue = JSONValue.intersection( lvalue, rvalue ) {
							dictionary[ key ] = ivalue
						}
					}
				}

				if dictionary.count > 0 {
					return JSONValue( dictionary )
				}
				else {
						return .None
				}

			// Arrays
			case (.JSONArray( let larray ), .JSONArray( let rarray )):
				var values = [JSONValue]()
				for lvalue in larray {
					for rvalue in rarray {
						if let ivalue = JSONValue.intersection( lvalue, rvalue ) {
							if !elementOf( ivalue, values ) {
								if ivalue == .JSONNull {
									println( "ivalue was found to be null for l: \(lhs) r: \(rhs)" )
								}
								values.append( ivalue )
							}
						}
						else {
							println( "ivalue was nil" )
						}
					}
				}

				return JSONValue( values )

			case (.JSONString( let lstring ), .JSONString( let rstring )):
				return lstring == rstring ? lhs : .None

			case (.JSONNumber( let lnumber ), .JSONNumber( let rnumber )):
				return lnumber == rnumber ? lhs : .None

			case (.JSONNumber( let lnumber ), .JSONDouble( let rnumber )):
				return lnumber.doubleValue == rnumber ? lhs : .None

			case (.JSONNumber( let lnumber ), .JSONInteger( let rnumber )):
				return lnumber.doubleValue == Double( rnumber ) ? lhs : .None

			case (.JSONDouble( let ldouble ), .JSONDouble( let rdouble )):
				return ldouble == rdouble ? lhs : .None

			case (.JSONDouble( let ldouble ), .JSONInteger( let rinteger )):
				return ldouble == Double( rinteger ) ? lhs : .None

			case (.JSONDouble( let ldouble ), .JSONNumber( let rnumber )):
				return ldouble == rnumber.doubleValue ? lhs : .None

			case (.JSONInteger( let linteger ), .JSONInteger( let rinteger )):
				return linteger == rinteger ? lhs : .None

			case (.JSONInteger( let linteger ), .JSONDouble( let rdouble )):
				return Double( linteger ) == rdouble ? lhs : .None

			case (.JSONInteger( let linteger ), .JSONNumber( let rnumber )):
				return Double( linteger ) == rnumber.doubleValue ? lhs : .None

			case (.JSONBool( let lbool ), .JSONBool( let rbool )):
				return lbool == rbool ? lhs : .None

			case (.JSONNull, .JSONNull):
				return JSON.null

			case (.Invalid, _), (_, .Invalid):
				return .None

			default:
				return .None
		}
	}

}

// MARK: export

extension JSONValue {

	private func values() -> NSObject {
		switch self {
			case .JSONDictionary( let dictionary ):
				var values = [String: AnyObject]()
				for (key, value) in dictionary {
					values[ key ] = value.values()
				}

				return values

			case .JSONArray( let array ):
				return array.map { $0.values() }

			case .JSONString( let string ):
				return NSString( string: string )

			case .JSONNumber( let number ):
				return number

			case .JSONDouble( let double ):
				return NSNumber( double: double )

			case .JSONInteger( let integer ):
				return NSNumber( integer: integer )

			case .JSONBool( let bool ):
				return NSNumber( bool: bool )

			case .JSONNull:
				return NSNull()

			case .Invalid:
				return NSNull()
		}
	}

	// MARK: encode

	public typealias DataResult = ResultOf<NSData>


	public func encode( options: NSJSONWritingOptions = nil ) -> DataResult {
		switch self {
			case .JSONDictionary, .JSONArray:
				var object = self.values()

				return JSONValue.encode( object, options )

			case .JSONString, .JSONNumber, .JSONDouble, .JSONInteger, .JSONBool, .JSONNull:
				var object = [ "@": self.values() ]

				return JSONValue.encode( object, options )

			case .Invalid:
				return DataResult( error: nil )
		}
	}

	public static func encode( object: [String: AnyObject], _ options: NSJSONWritingOptions = nil ) -> DataResult {
		var error: NSError? = nil
		var data = NSJSONSerialization.dataWithJSONObject( object, options: options, error: &error )

		if error != nil {
			println( error )

			return DataResult( error: Error( error: error! ) )
		}

		return DataResult( value: data! )
	}

	public static func encode( object: [AnyObject], _ options: NSJSONWritingOptions = nil ) -> DataResult {
		var error: NSError? = nil
		var data = NSJSONSerialization.dataWithJSONObject( object, options: options, error: &error )

		if error != nil {
			println( error )

			return DataResult( error: Error( error: error! ) )
		}

		return DataResult( value: data! )
	}

	private static func encode( object: AnyObject, _ options: NSJSONWritingOptions = nil ) -> DataResult {
		if NSJSONSerialization.isValidJSONObject( object ) {
			var error: NSError? = nil
			var data = NSJSONSerialization.dataWithJSONObject( object, options: options, error: &error )

			if error != nil {
				println( error )

				return DataResult( error: Error( error: error! ) )
			}

			return DataResult( value: data! )
		}

		return DataResult( error: Error( name: "encode( object ) failed", domain: "JSON" ) )
	}

}

// MARK: Equatable

extension JSONValue : Equatable {}

public func ==( lhs: JSONValue, rhs: JSONValue ) -> Bool {
	switch (lhs, rhs) {
		case (.JSONDictionary( let ldictionary ), .JSONDictionary( let rdictionary )):
			return ldictionary == rdictionary

		case (.JSONArray( let larray ), .JSONArray( let rarray )):
			return larray == rarray

		case (.JSONString( let lstring ), .JSONString( let rstring )):
			return lstring == rstring

		case (.JSONNumber( let lnumber ), .JSONNumber( let rnumber )):
			return lnumber == rnumber

		case (.JSONNumber( let lnumber ), .JSONDouble( let rnumber )):
			return lnumber.doubleValue == rnumber

		case (.JSONNumber( let lnumber ), .JSONInteger( let rnumber )):
			return lnumber.doubleValue == Double( rnumber )

		case (.JSONDouble( let lnumber ), .JSONDouble( let rnumber )):
			return lnumber == rnumber

		case (.JSONDouble( let lnumber ), .JSONInteger( let rnumber )):
			return lnumber == Double( rnumber )

		case (.JSONDouble( let lnumber ), .JSONNumber( let rnumber )):
			return lnumber == rnumber.doubleValue

		case (.JSONInteger( let lnumber ), .JSONInteger( let rnumber )):
			return lnumber == rnumber

		case (.JSONInteger( let lnumber ), .JSONDouble( let rnumber )):
			return Double( lnumber ) == rnumber

		case (.JSONInteger( let lnumber ), .JSONNumber( let rnumber )):
			return Double( lnumber ) == rnumber.doubleValue

		case (.JSONBool( let lbool ), .JSONBool( let rbool )):
			return lbool == rbool

		case (.JSONNull, .JSONNull):
			return true

		case (.Invalid, _), (_, .Invalid):
			// invalid entries are invalid and not considered further
			return false

		default:
			return false
	}
}

// MARK: Printable

extension JSONValue : Printable {

	public var description: String {
		switch self {
			case .JSONDictionary( let dictionary ):
				return "JSONDictionary( \(dictionary) )"

			case .JSONArray( let array ):
				return "JSONArray( \(array) )"

			case .JSONString( let string ):
				return "JSONString( \(string) )"

			case .JSONNumber( let number ):
				return "JSONNumber( \(number) )"

			case .JSONDouble( let double ):
				return "JSONDouble( \(double) )"

			case .JSONInteger( let integer ):
				return "JSONInteger( \(integer) )"

			case .JSONBool( let bool ):
				return "JSONBool( \(bool) )"

			case .JSONNull:
				return "JSONNull"

			case .Invalid( let value ):
				if let value: AnyObject = value {
					return "JSONInvalid( \(value) )"
				}
				else {
					return "JSONInvalid"
				}
		}
	}

	public func prettyprint() -> String {
		let result = self.encode( options: NSJSONWritingOptions.PrettyPrinted )
		switch result {
			case .Success:
				if let data = result.value {
					if let string = NSString( data: data, encoding: NSUTF8StringEncoding ) {
						return string.stringByReplacingOccurrencesOfString( "\\/", withString: "/" )
					}
				}

				return "JSONNull"

			case .Failure:
				return "JSONNull"
		}
	}
	
}
