//
//  author:       fabrice truillot de chambrier
//  created:      23.03.2015
//
//  license:      see license.txt
//
//  © 2015-2015, men in silicium sàrl
//


import Foundation
import Dispatch


public class DispatchBlock {

	public var function: (() -> Void)?
	public var queue: DispatchQueue?

	private var dispatched: Bool = false
	private var executing: Bool = false


	public init( function: () -> Void ) {
		self.function = function
		self.queue = nil
	}

 	private func async() -> Bool {
		if self.dispatched { return true }

		if let queue = self.queue, let function = self.function {
			self.dispatched = true
			queue.async {
				self.executing = true
				function()
				self.executing = false
				self.function = nil
			}

			return true
		}

		return false
	}

}
