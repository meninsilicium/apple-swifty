//
//  author:       fabrice truillot de chambrier
//  created:      03.09.2014
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


#import "Exceptions.h"


@implementation ExceptionsHelper

+ (NSException*) try: (void (^)(void)) block
{
	if ( block == nil ) return nil;

	@try {
		block();
	}
	@catch( NSException* exception ) {
		return exception;
	}
	@catch(...) {
		// no exception will escape from here
	}

	return nil;
}

+ (void) try: (void (^)(void)) tryBlock catch: (void (^)(NSException*)) catchBlock finally: (void (^)(void)) finallyBlock
{
	if ( tryBlock == nil ) return;

	@try {
		tryBlock();
	}
	@catch( NSException* exception ) {
		catchBlock( exception );
	}
	@catch(...) {
		// no exception will escape from here
	}
	@finally {
		if ( finallyBlock != nil ) {
			finallyBlock();
		}
	}
}

+ (void) throw: (NSException*) exception
{
	if ( exception != nil ) {
		@throw exception;
	}
}

+ (void) throw: (NSString*) message reason: (NSString*) reason
{
	@throw [[NSException alloc] initWithName: message reason: reason userInfo: nil];
}

@end
