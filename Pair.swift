//
//  author:       fabrice truillot de chambrier
//  created:      19.01.2015
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


// MARK: - Pair

public struct Pair<T> {

	private let left: T
	private let right: T

	// MARK: initialization

	public init( _ value: T ) {
		self.left = value
		self.right = value
	}

	public init( left: T, right: T ) {
		self.left = left
		self.right = right
	}

	public init( tuple: (T, T) ) {
		( self.left, self.right ) = tuple
	}

	public func tuple() -> (T, T) {
		return (self.left, self.right)
	}

}

// Iterable

extension Pair : Iterable {

	public func foreach( @noescape function: (T) -> Void ) {
		function( self.left )
		function( self.right )
	}

	public func foreach( @noescape function: (Int, T) -> Void ) {
		function( 0, self.left )
		function( 1, self.right )
	}

}

// Functor

extension Pair : Functor {

	typealias MappedType = Any
	typealias MappedFunctorType = Pair<MappedType>


	public func map<R>( @noescape transform: (T) -> R ) -> Pair<R> {
		return Pair<R>( left: transform( self.left ), right: transform( self.right ) )
	}

}

public func <^><T, R>( pair: Pair<T>, @noescape transform: (T) -> R ) -> Pair<R> {
	return pair.map( transform )
}

// Pointed

extension Pair : Pointed {

	public static func pure( value: T ) -> Pair<T> {
		return Pair<T>( value )
	}

}

// Applicative

extension Pair : Applicative {

	typealias ApplicativeType = Pair<(T) -> MappedType>


	public static func apply<R>( transform: Pair<(T) -> R>, value: Pair<T> ) -> Pair<R> {
		return Pair<R>( left: transform.left( value.left ), right: transform.right( value.right ) )
	}

}

/*
func <*><T, R>( transform: Pair<(T) -> R>, value: Pair<T> ) -> Pair<R> {
	return Pair.apply( transform, value: value )
}
*/

// Monad

extension Pair : Monad {

	public func fmap<R>( @noescape transform: (T) -> Pair<R> ) -> Pair<R> {
		return Pair<R>.flatten( self.map( transform ) )
	}

	public static func flatten( pair: Pair<Pair<T>> ) -> Pair<T> {
		return Pair<T>( left: pair.left.left, right: pair.right.right )
	}

}

/*
public func >>*<T, R>( value: Pair<T>, transform: (T) -> Pair<R> ) -> Pair<R> {
	return value.fmap( transform )
}
*/
