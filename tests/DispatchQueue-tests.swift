//
//  author:       fabrice truillot de chambrier
//
//  © 2015-2015, men in silicium sàrl
//


import XCTest


class DispatchQueueTests : XCTestCase {

	func test_context() {

		let queue = DispatchQueue( label: "Dispatch Queue Tests", concurrent: true )

		println( queue.label )

		class Context {
			var value: Int
			init() { self.value = 10; println( "allocated" ) }
			deinit { println( "deallocated" ) }
		}

		queue.context = Context();

		XCTAssertTrue( (queue.context as! Context).value == 10, "(queue.context as! Context).value == 10" )

		(queue.context as! Context).value += 10
		XCTAssertTrue( (queue.context as! Context).value == 20, "(queue.context as! Context).value == 20" )

		(queue.context as! Context).value += 10
		XCTAssertTrue( (queue.context as! Context).value == 30, "(queue.context as! Context).value == 30" )

	}

	func test_dispatch_async() {
		let expectation = self.expectationWithDescription( "async" )

		let queue = DispatchQueue( label: "Dispatch Queue Tests", concurrent: true, target: .BackgroundQueue )

		var done = false
		queue.async {
			done = true
			expectation.fulfill()
		}

		self.waitForExpectationsWithTimeout( 10 ) { (error) -> Void in
		}
		XCTAssertTrue( done )

	}

	func test_dispatch_sync() {
		let expectation = self.expectationWithDescription( "sync" )

		let queue = DispatchQueue( label: "Dispatch Queue Tests", concurrent: true, target: .BackgroundQueue )

		var done = false
		queue.sync { () -> Void in
			done = true
			expectation.fulfill()
		}

		XCTAssertTrue( done )
		self.waitForExpectationsWithTimeout( 10 ) { (error) -> Void in
		}

	}

	func test_dispatch_after() {
		let expectation = self.expectationWithDescription( "after" )

		let queue = DispatchQueue( label: "Dispatch Queue Tests", concurrent: true, target: .BackgroundQueue )

		var done = false
		let start = NSDate.timeIntervalSinceReferenceDate()
		let after: Double = 1
		queue.after( DispatchTime( after: after ) ) {
			done = true
			expectation.fulfill()
		}

		XCTAssertFalse( done )
		self.waitForExpectationsWithTimeout( 10 ) { (error) -> Void in
		}
		let stop = NSDate.timeIntervalSinceReferenceDate()
		XCTAssertTrue( done )
		XCTAssertEqualWithAccuracy( start + after, stop, 0.1 )

	}

	func test_dispatch_suspend() {
		let expectation = self.expectationWithDescription( "resume" )

		let queue = DispatchQueue( label: "Dispatch Queue Tests", concurrent: true, target: .BackgroundQueue )
		queue.suspend()

		var done = false
		let after: Double = 1
		queue.async { () -> Void in
			done = true
			expectation.fulfill()
		}
		XCTAssertFalse( done )

		queue.resume()

		self.waitForExpectationsWithTimeout( 10 ) { (error) -> Void in
		}
		XCTAssertTrue( done )

	}

}
