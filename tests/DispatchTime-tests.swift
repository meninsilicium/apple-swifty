//
//  author:       fabrice truillot de chambrier
//
//  © 2015-2015, men in silicium sàrl
//


import XCTest


class DispatchTimeTests : XCTestCase {

	func test_time_now() {

		let time = DispatchTime()

		XCTAssertTrue( time.isNow() )
		XCTAssertTrue( time.time != 0 )

	}

	func test_time_absolute() {

		let when = NSDate( timeIntervalSinceNow: 10 )
		let time = DispatchTime( at: when )

		XCTAssertTrue( time.isAbsolute() )
		XCTAssertTrue( time.time != 0 )
		XCTAssertTrue( time.date.isEqualToDate( when ) )

	}

	func test_time_relative() {

		let time = DispatchTime( after: 10 )

		XCTAssertTrue( time.isRelative() )
		XCTAssertTrue( time.time != 0 )

	}

	func test_time_accuracy() {
		let delta: Double = 100
		let when = NSDate( timeIntervalSinceNow: delta )
		let accuracy: Double = 1 / 1000

		let time₁ = DispatchTime()
		let time₂ = DispatchTime( at: when )
		let time₃ = DispatchTime( after: delta )

		// time₁ == time₂ - delta
		XCTAssertEqualWithAccuracy( time₁.date.timeIntervalSinceReferenceDate, time₂.date.timeIntervalSinceReferenceDate - delta, accuracy )
		// time₁ == time₃ - delta
		XCTAssertEqualWithAccuracy( time₁.date.timeIntervalSinceReferenceDate, time₃.date.timeIntervalSinceReferenceDate - delta, accuracy )
		// time₂ == time₃
		XCTAssertEqualWithAccuracy( time₂.date.timeIntervalSinceReferenceDate, time₃.date.timeIntervalSinceReferenceDate, accuracy )
		// time₂ == when
		XCTAssertEqualWithAccuracy( time₂.date.timeIntervalSinceReferenceDate, when.timeIntervalSinceReferenceDate, accuracy )
		// time₃ == when
		XCTAssertEqualWithAccuracy( time₃.date.timeIntervalSinceReferenceDate, when.timeIntervalSinceReferenceDate, accuracy )
	}

}
