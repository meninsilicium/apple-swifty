//
//  author:       fabrice truillot de chambrier
//
//  © 2015-2015, men in silicium sàrl
//


import XCTest


class ResultsTests : XCTestCase {

	func test_Result() {

		XCTAssert( Result().succeeded, "Result().succeeded" )
		XCTAssert( Result( error: nil ).failed, "Result( error: nil ).failed" )

	}

	func test_ResultOf() {

		let yes = ResultOf<Boolean>( true )
		let no = ResultOf<Boolean>( false )
		let error = ResultOf<Boolean>( error: Error( name: "Error", domain: "Results Tests" ) )

		XCTAssert( yes.succeeded, "yes.succeeded" )
		XCTAssert( yes.value == true, "yes.value == true" )
		XCTAssert( no.succeeded, "no.succeeded" )
		XCTAssert( no.value == false, "no.value == false" )

		XCTAssert( !error.succeeded, "!error.succeeded" )
		XCTAssert( error.failed, "error.failed" )

	}

	func test_ResultOf_Iterable() {

		let yes = ResultOf<Boolean>( true )
		let no = ResultOf<Boolean>( false )
		let error = ResultOf<Boolean>( error: Error( name: "Error", domain: "Results Tests" ) )

		var pass₁ = false

		yes.foreach { (value: Boolean) in
			if value { pass₁ = true }
		}
		XCTAssert( pass₁, "yes.foreach" )

		var pass₂ = false
		no.foreach { (index: Int, value: Boolean) in
			if index == 0 && !value.boolValue { pass₂ = true }
		}
		XCTAssert( pass₂, "no.foreach" )

	}

	func test_ResultOf_Monad() {

		let yes = ResultOf<Boolean>( true )
		let no = ResultOf<Boolean>( false )

		var success = yes.fmap { (value: Boolean) -> ResultOf<Boolean> in
			return value ? ResultOf( value ) : ResultOf<Boolean>( error: Error( name: "Error", domain: "Results Tests" ) )
		}
		XCTAssert( success.succeeded, "success.succeeded" )
		XCTAssert( success.value == true, "success.value == true" )

		var error = no.fmap { (value: Boolean) -> ResultOf<Boolean> in
			return value ? ResultOf( value ) : ResultOf<Boolean>( error: Error( name: "Error", domain: "Results Tests" ) )
		}
		XCTAssert( error.failed, "error.failed" )
		XCTAssert( error.value == nil, "error.value == nil" )

	}

	func test_ResultOf_Chaining() {

		let yes = ResultOf<Boolean>( true )
		let no = ResultOf<Boolean>( false )
		let error = ResultOf<Boolean>( error: Error( name: "Error", domain: "Results Tests" ) )

		var pass₁ = false

		yes.onSuccess { (value) in
			if value { pass₁ = true }
		}
		.onFailure { (error) in
			pass₁ = false
		}
		XCTAssert( pass₁, "yes.onSuccess" )

		var pass₂ = false
		no.onSuccess { (value) in
			if !value { pass₂ = true }
		}
		.onFailure { (error) in
			pass₂ = false
		}
		XCTAssert( pass₂, "no.onSuccess" )

		var pass₃ = false
		error.onSuccess { (value) in
			pass₃ = false
		}
		.onFailure { (error) in
			pass₃ = true
		}
		XCTAssert( pass₃, "error.onFailure" )

	}

}
