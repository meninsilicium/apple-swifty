//
//  author:       fabrice truillot de chambrier
//
//  © 2015-2015, men in silicium sàrl
//


import XCTest


class FunctionsTests : XCTestCase {

	func test_tap() {

		struct V {
			var v: Int
			init() { v = 0 }
		}

		let v₁ = tap( V() ) { (inout v: V) in
			v.v = 10
		}

		let v₂ = tap( v₁ ) { (inout v: V) in
			v.v += 90
		}

		XCTAssert( v₁.v == 10, "v₁.v == 10" )
		XCTAssert( v₂.v == 100, "v₂.v == 100" )

	}

}
