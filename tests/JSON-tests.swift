//
//  author:       fabrice truillot de chambrier
//
//  © 2015-2015, men in silicium sàrl
//


import XCTest


class JSONTests : XCTestCase {

	func test_JSON_Initialization() {
	}

	func test_isSetable() {

		var json: JSONValue = [
			"coordinate" : [
				"latitude" : 46.2050295,
				"longitude" : 6.144,
				"city" : "Geneva"
			],
			"landmarks" : [
				"palais de justice",
				"université"
			],
			"source" : "literals"
		]

		XCTAssertTrue( json.isSetable( JSONPath() ) )
		XCTAssertTrue( json.isSetable( JSONPath( "coordinate" ) ) )
		XCTAssertTrue( json.isSetable( JSONPath( "coordinate", "city" ) ) )
		XCTAssertFalse( json.isSetable( JSONPath( "coordinate", "city", "block" ) ) )
		XCTAssertTrue( json.isSetable( JSONPath( "landmarks", 0 ) ) )
		XCTAssertTrue( json.isSetable( JSONPath( "landmarks", 1 ) ) )
		XCTAssertFalse( json.isSetable( JSONPath( "landmarks", 2 ) ) )
		XCTAssertFalse( json.isSetable( JSONPath( "landmarks", 0, "alternate" ) ) )
		XCTAssertTrue( json.isSetable( JSONPath( "source" ) ) )

	}

	func test_array_operations() {
		var json: JSONValue = []

		json.append( JSONValue( "0" ) )
		json.append( JSONValue( 1 ) )

		XCTAssertTrue( json[0]!.string == "0" )
		XCTAssertTrue( json[1]!.integer == 1 )

		json.extend( [ JSONValue( 2 ), JSONValue( true ) ] )

		XCTAssertTrue( json[2]!.integer == 2 )
		XCTAssertTrue( json[3]!.bool == true )

	}

}
