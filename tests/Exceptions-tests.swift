//
//  author:       fabrice truillot de chambrier
//
//  © 2015-2015, men in silicium sàrl
//


import XCTest


class ExceptionsTests : XCTestCase {

	func test_attempt_return() {

		let result = attempt { () -> Bool in
			return true
		}

		XCTAssert( result.succeeded, "attempt succeeded" )

	}
	
	func test_attempt_throw() {

		let result = attempt { () -> Bool in
			Exceptions.throw( message: "test", reason: "testing" )

			return false
		}

		XCTAssert( result.failed, "attempt failed" )

	}

	// Swift doesn't like exceptions. Unfortunately, some parts of Cocoa uses them, which may cause memory leaks.
	// Below is a prime example of a leak, with a test to detect if it happens to be fixed in some upcoming Swift release.
	// (I bet Apple will not fix it as it would require to support exceptions at the Swift runtime level if not at the language level.)
	func test_attempt_leak() {

		class Count {
			var count = 0
			func inc() { self.count += 1 }
			func dec() { self.count -= 1 }
		}

		class Counter {
			var count: Count
			init( _ count: Count ) { self.count = count; count.inc() }
			deinit { count.dec() }
		}

		var count = Count()

		// no exception, no leak

		let result₁ = attempt { () -> Bool in
			var counter = Counter( count )
			XCTAssert( count.count == 1, "count == 1" )

			return true
		}
		XCTAssert( result₁.succeeded, "attempt succeeded" )
		XCTAssert( count.count == 0, "count == 0" )

		// exception, leak

		let result₂ = attempt { () -> Bool in
			var counter = Counter( count )
			XCTAssert( count.count == 1, "count == 1" )
			Exceptions.throw( message: "test", reason: "testing" )

			return false
		}
		XCTAssert( result₂.failed, "attempt failed" )
		XCTAssert( count.count == 1, "count == 1" )

	}
	
}
