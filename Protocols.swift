//
//  author:       fabrice truillot de chambrier
//  created:      15.02.2015
//
//  license:      See license.txt
//
//  © 2015-2015, men in silicium sàrl
//


// MARK: Namespace

// A protocol to annotate classes (or structs) used as namespaces / submodules

public protocol Submodule {}


// MARK: Iterable
// Containers that can be iterated through all elements

public protocol Iterable {

	typealias ValueType


	// foreach( function: (value) -> Void )
	func foreach( @noescape function: (ValueType) -> Void )
	// foreach( function: (index, value) -> Void )
	func foreach( @noescape function: (Int, ValueType) -> Void )

}

// Same protocol, but internal due to a compiler bug with extensions for types from another module

protocol Iterable_ {

	typealias ValueType


	func foreach( @noescape function: (ValueType) -> Void )
	func foreach( @noescape function: (Int, ValueType) -> Void )
	
}


// MARK: -
// MARK: Enumerable

public protocol Enumerable {

	typealias ValueType

	// enumerate( function: (index, value) -> continue )
	// function:
	//   index: index in the collection
	//   value: value in the collection
	//   continue: continue to the next value
	func enumerate( @noescape function: (Int, ValueType) -> Bool )

}

protocol Enumerable_ {

	typealias ValueType

	func enumerate( @noescape function: (Int, ValueType) -> Bool )
	
}

// MARK: -
// MARK: Freezable

// A Freezable type is mutable until frozen.

protocol Freezable {

	mutating func freeze() -> Bool

	var isFrozen: Bool { get }

}
