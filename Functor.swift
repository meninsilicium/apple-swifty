//
//  author:       fabrice truillot de chambrier
//  created:      19.01.2015
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


// References:
// https://www.haskell.org/haskellwiki/Typeclassopedia
// https://www.haskell.org/wikiupload/8/85/TMR-Issue13.pdf
// https://themonadreader.wordpress.com/


// map

infix operator <^> { associativity left precedence 150 }


// MARK: Functor

// FunctorSpecifics

public protocol FunctorSpecifics {

	typealias ValueType

	typealias MappedType
	typealias MappedFunctorType = Generic1<MappedType>


	func map( @noescape transform: (ValueType) -> MappedType ) -> MappedFunctorType // Haskell's fmap

	func <^>( value: Self, @noescape transform: (ValueType) -> MappedType ) -> MappedFunctorType

}

public func <^><T: FunctorSpecifics>( value: T, @noescape transform: (T.ValueType) -> T.MappedType ) -> T.MappedFunctorType {
	return value.map( transform )
}

// Functor

public protocol Functor : FunctorSpecifics {}

public func <^><T: Functor>( value: T, @noescape transform: (T.ValueType) -> T.MappedType ) -> T.MappedFunctorType {
	return value.map( transform )
}


// MARK: -
// MARK: internal Functor

// FunctorSpecifics

protocol FunctorSpecifics_ {

	typealias ValueType

	typealias MappedType
	typealias MappedFunctorType = Generic1<MappedType>


	func map( @noescape transform: (ValueType) -> MappedType ) -> MappedFunctorType // Haskell's fmap

	func <^>( value: Self, @noescape transform: (ValueType) -> MappedType ) -> MappedFunctorType

}

func <^><T: FunctorSpecifics_>( value: T, @noescape transform: (T.ValueType) -> T.MappedType ) -> T.MappedFunctorType {
	return value.map( transform )
}

// Functor

protocol Functor_ : FunctorSpecifics_ {}

func <^><T: Functor_>( value: T, @noescape transform: (T.ValueType) -> T.MappedType ) -> T.MappedFunctorType {
	return value.map( transform )
}


// The following protocol is supposed to be valid and simple, but any implementation crashes the compiler with a segfault 11.
// So I redefined the protocol using associated types only. It is more convoluted, but it works without crashing the compiler.

private protocol FunctorGeneric {

	typealias ValueType


	func map<R: Functor>( @noescape transform: ValueType -> R.ValueType ) -> R

	func <^><R: Functor>( value: Self, @noescape transform: (ValueType) -> R.ValueType ) -> R

}
