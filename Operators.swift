//
//  author:       fabrice truillot de chambrier
//  created:      25.07.2014
//
//  license:      See license.txt
//
//  © 2014-2015, men in silicium sàrl
//


import Foundation
import CoreGraphics
import Darwin.C.math


// Documentation: http://www.unicode.org/charts/
// Documentation: http://www.unicode.org/charts/PDF/U2200.pdf

// MARK: operator definitions

// not
prefix operator ¬ {}

// complement
prefix operator ∁ {}

// and
infix operator ⋀ { associativity left precedence 120 }

// nand
infix operator !&& { associativity left precedence 120 }
infix operator ⊼ { associativity left precedence 120 }

// or
infix operator ⋁ { associativity left precedence 110 }

// nor
infix operator !|| { associativity left precedence 110 }
infix operator ⊽ { associativity left precedence 110 }

// xor
infix operator ^^ { associativity left precedence 110 }
infix operator ⊻ { associativity left precedence 110 }

// nxor
infix operator !^^ { associativity left precedence 110 }

// element of
infix operator ∈ { associativity none precedence 130 }

// not element of
infix operator !∈ { associativity none precedence 130 }
infix operator ∉ { associativity none precedence 130 }

// contains
infix operator ∋ { associativity none precedence 130 }

// doesn't contain
infix operator !∋ { associativity none precedence 130 }
infix operator ∌ { associativity none precedence 130 }

// exists
prefix operator ∃ {}

// doesn't exist
prefix operator !∃ {}
prefix operator ∄ {}


// MARK: not, ¬

func not( value: Bool ) -> Bool {
	return !value
}

func not<T: BooleanType>( value: T ) -> Bool {
	return !value
}

prefix func ¬( value: Bool ) -> Bool {
	return !value
}

prefix func ¬<T: BooleanType>( value: T ) -> Bool {
	return !value
}

// MARK: bit not

func bitnot( value: Int ) -> Int {
	return ~value
}

func bitnot( value: UInt ) -> UInt {
	return ~value
}

// MARK: complement, ∁

public prefix func ∁( value: Int ) -> Int {
	return ~value
}

public prefix func ∁( value: UInt ) -> UInt {
	return ~value
}

// MARK: - and, ⋀

func and( lhs: Bool, @autoclosure rhs: () -> Bool ) -> Bool {
	return lhs && rhs
}

func and<L: BooleanType>( lhs: L, @autoclosure rhs: () -> Bool ) -> Bool {
	return lhs && rhs
}

func and<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return lhs && rhs
}

func ⋀( lhs: Bool, @autoclosure rhs: () -> Bool ) -> Bool {
	return lhs && rhs
}

func ⋀<L: BooleanType>( lhs: L, @autoclosure rhs: () -> Bool ) -> Bool {
	return lhs && rhs
}

func ⋀<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return lhs && rhs
}

// MARK: nand, !&&, ⊼

func nand( lhs: Bool, @autoclosure rhs: () -> Bool ) -> Bool {
	return !(lhs && rhs)
}

func nand<L: BooleanType>( lhs: L, @autoclosure rhs: () -> Bool ) -> Bool {
	return !(lhs && rhs)
}

func nand<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return !(lhs && rhs)
}

public func !&&( lhs: Bool, @autoclosure rhs: () -> Bool ) -> Bool {
	return !(lhs && rhs)
}

func ⊼( lhs: Bool, @autoclosure rhs: () -> Bool ) -> Bool {
	return !(lhs && rhs)
}

func ⊼<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return !(lhs && rhs)
}

// MARK: - bit and

func band( lhs: Int, rhs: Int ) -> Int {
	return lhs & rhs
}

func band( lhs: UInt, rhs: UInt ) -> UInt {
	return lhs & rhs
}

// MARK: - or, ⋁

func or( lhs: Bool, @autoclosure rhs: () -> Bool ) -> Bool {
	return lhs || rhs
}

func or<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return lhs || rhs
}

func ⋁( lhs: Bool, @autoclosure rhs: () -> Bool ) -> Bool {
	return lhs || rhs
}

func ⋁<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return lhs || rhs
}

// MARK: nor, !||, ⊽

func nor( lhs: Bool, @autoclosure rhs: () -> Bool ) -> Bool {
	return !(lhs || rhs)
}

func nor<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return !(lhs || rhs)
}

public func !||( lhs: Bool, @autoclosure rhs: () -> Bool ) -> Bool {
	return !(lhs || rhs)
}

func ⊽( lhs: Bool, @autoclosure rhs: () -> Bool ) -> Bool {
	return !(lhs || rhs)
}

public func !||<L: BooleanType>( lhs: L, @autoclosure rhs: () -> Bool ) -> Bool {
	return !(lhs || rhs)
}

func ⊽<L: BooleanType>( lhs: L, @autoclosure rhs: () -> Bool ) -> Bool {
	return !(lhs || rhs)
}

public func !||<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return !(lhs || rhs)
}

func ⊽<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return !(lhs || rhs)
}

// MARK: - bit or

func bor( lhs: Int, rhs: Int ) -> Int {
	return lhs | rhs
}

func bor( lhs: UInt, rhs: UInt ) -> UInt {
	return lhs | rhs
}

// MARK: - xor, ^^, ⊻

func xor( lhs: Bool, rhs: Bool ) -> Bool {
	return (lhs || rhs) && !(lhs && rhs)
	//return (lhs && !rhs) || (!lhs && rhs)
}

func xor<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return (lhs || rhs) && !(lhs && rhs)
}

public func ^^( lhs: Bool, rhs: Bool ) -> Bool {
	return (lhs || rhs) && !(lhs && rhs)
}

public func ^^<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return (lhs || rhs) && !(lhs && rhs)
}

func ⊻( lhs: Bool, rhs: Bool ) -> Bool {
	return (lhs || rhs) && !(lhs && rhs)
}

func ⊻<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return (lhs || rhs) && !(lhs && rhs)
}

// MARK: - nxor, !^^

func nxor( lhs: Bool, rhs: Bool ) -> Bool {
	return !(lhs ^^ rhs)
}

func nxor<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return !(lhs ^^ rhs)
}

public func !^^( lhs: Bool, rhs: Bool ) -> Bool {
	return !(lhs ^^ rhs)
}

public func !^^<L: BooleanType, R: BooleanType>( lhs: L, @autoclosure rhs: () -> R ) -> Bool {
	return !(lhs ^^ rhs)
}

// MARK: - elementOf, ∈

func elementOf<S: SequenceType where S.Generator.Element: Equatable>( value: S.Generator.Element, sequence: S ) -> Bool {
	return contains( sequence, value )
}


public func ∈<S: SequenceType where S.Generator.Element: Equatable>( value: S.Generator.Element, sequence: S ) -> Bool {
	return contains( sequence, value )
}

func !∈<S: SequenceType where S.Generator.Element: Equatable>( value: S.Generator.Element, sequence: S ) -> Bool {
	return !contains( sequence, value )
}

public func ∉<S: SequenceType where S.Generator.Element: Equatable>( value: S.Generator.Element, sequence: S ) -> Bool {
	return !contains( sequence, value )
}

// MARK: - contains, ∋

public func ∋<S: SequenceType where S.Generator.Element: Equatable>( sequence: S, value: S.Generator.Element ) -> Bool {
	return contains( sequence, value )
}

public func !∋<S: SequenceType where S.Generator.Element: Equatable>( sequence: S, value: S.Generator.Element ) -> Bool {
	return !contains( sequence, value )
}

public func ∌<S: SequenceType where S.Generator.Element: Equatable>( sequence: S, value: S.Generator.Element ) -> Bool {
	return !contains( sequence, value )
}

// MARK: - exists, ∃

func exists<T>( value: T? ) -> Bool {
	return (value != nil) && !(value is NSNull)
}

func isNil<T>( value: T? ) -> Bool {
	return (value == nil) || (value is NSNull)
}

func isNull<T>( value: T? ) -> Bool {
	return (value == nil) || (value is NSNull)
}

public prefix func ∃<T>( value: T? ) -> Bool {
	return value != nil
}

public prefix func !∃<T>( value: T? ) -> Bool {
	return value == nil
}

public prefix func ∄<T>( value: T? ) -> Bool {
	return value == nil
}

// MARK: - promote, ➚

prefix operator ➚ {}

prefix func ➚( value: Int8 ) -> Int {
	return Int( value )
}

prefix func ➚( value: Int16 ) -> Int {
	return Int( value )
}

prefix func ➚( value: Int32 ) -> Int {
	return Int( value )
}

prefix func ➚( value: Int ) -> Int {
	return Int( value )
}

prefix func ➚( value: UInt8 ) -> UInt {
	return UInt( value )
}

prefix func ➚( value: UInt16 ) -> UInt {
	return UInt( value )
}

prefix func ➚( value: UInt32 ) -> UInt {
	return UInt( value )
}

prefix func ➚( value: UInt ) -> UInt {
	return UInt( value )
}

prefix func ➚( value: Float ) -> Double {
	return Double( value )
}

prefix func ➚( value: Double ) -> Double {
	return Double( value )
}

// MARK: - demote, ➘

prefix operator ➘ {}

prefix func ➘( value: Int ) -> Int {
	return Int( value )
}

prefix func ➘( value: Int64 ) -> Int {
	return Int( value )
}

prefix func ➘( value: UInt ) -> UInt {
	return UInt( value )
}

prefix func ➘( value: UInt64 ) -> UInt {
	return UInt( value )
}

prefix func ➘( value: Float ) -> Float {
	return Float( value )
}

prefix func ➘( value: Double ) -> Float {
	return Float( value )
}

prefix func ➘( value: CGFloat ) -> Float {
	return Float( value )
}

// MARK: - square root, √

prefix operator √ {}

public prefix func √( value: Float ) -> Float {
	return sqrtf( value )
}

public prefix func √( value: Double ) -> Double {
	return sqrt( value )
}

// MARK: cube root, ∛

prefix operator ∛ {}

prefix func ∛( value: Float ) -> Float {
	return cbrtf( value )
}

prefix func ∛( value: Double ) -> Double {
	return cbrt( value )
}
