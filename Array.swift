//
//  author:       fabrice truillot de chambrier
//  created:      29.01.2015
//
//  license:      see license.txt
//
//  © 2015-2015, men in silicium sàrl
//


import struct ObjectiveC.ObjCBool
import class Foundation.NSArray
import class Foundation.NSMutableArray
import class Foundation.NSIndexSet
import struct Foundation.NSRange


extension Array {

	var tail: [T] {
		return [T]( self[1 ..< self.count] )
	}

}

// Iterable

extension Array : Iterable_ {

	func foreach( @noescape function: (T) -> Void ) {
		for element in self {
			function( element )
		}
	}

	func foreach( @noescape function: (Int, T) -> Void ) {
		for (index, element) in Swift.enumerate( self ) {
			function( index, element )
		}
	}

}

extension Array : Enumerable_ {

	func enumerate( @noescape function: (Int, T) -> Bool ) {
		for (index, value) in Swift.enumerate( self ) {
			let next = function( index, value )
			if !next { break }
		}
	}

}

@transparent
private func _enumerate<S: SequenceType>( values: S ) -> EnumerateSequence<S> {
	return Swift.enumerate( values )
}

/// Array extension providing the usual monad functions

// MARK: Functor

extension Array : Functor_ {

	typealias MappedType = Any
	typealias MappedFunctorType = [MappedType]

	// functor map, <^>
	func map<R>( @noescape transform: (T) -> R ) -> [R] {
		var array = [R]()
		self.foreach { (value) in
			array.append( transform( value ) )
		}

		return array
	}

}

public func <^><T, R>( array: [T], @noescape transform: (T) -> R ) -> [R] {
	return array.map( transform )
}

// Pointed

extension Array : Pointed_ {

	static func pure( value: T ) -> [T] {
		return [value]
	}

	init( _ value: T ) {
		self.init( CollectionOfOne( value ) )
	}

}

// Applicative

extension Array : Applicative_ {

	typealias ApplicativeType = [(T) -> MappedType]


	// applicative apply, <*>
	static func apply<R>( transform: [(T) -> R], value: [T] ) -> [R] {
		var array = [R]()
		let count = min( transform.count, value.count )
		for index in 0 ..< count {
			array.append( transform[ index ]( value[ index ] ) )
		}

		return array
	}

}

// applicative apply, <*>
// using the default implementation

// Monad

extension Array : Monad_ {

	// monad bind, >>*
	func fmap<R>( @noescape transform: (T) -> [R] ) -> [R] {
		return [R].flatten( self.map( transform ) )
	}

	// flatten
	static func flatten( value: [[T]] ) -> [T] {
		var array = [T]()
		for element in value {
			array.extend( element )
		}

		return array
	}

}

// monad bind, >>*
/*
public func >>*<T, R>( value: [T], @noescape transform: (T) -> [R] ) -> [R] {
	return value.fmap( transform )
}

public func *<<<T, R>( @noescape transform: (T) -> [R], value: [T] ) -> [R] {
	return value.fmap( transform )
}
*/

// MARK: -
// MARK: NSArray

extension NSArray {

	typealias ValueType = AnyObject


	public var tail: NSArray {
		return self.objectsAtIndexes( NSIndexSet( indexesInRange: NSRange( location: 1, length: self.count - 1 ) ) )
	}

	public var last: AnyObject? {
		return self.lastObject
	}

}

// Iterable

extension NSArray {

	public func foreach( @noescape function: (AnyObject) -> Void ) {
		for value in self {
			function( value )
		}
	}

}

extension NSArray : Enumerable {

	public func enumerate( @noescape function: (Int, AnyObject) -> Bool ) {
		for index in 0 ..< self.count {
			let next = function( index, self[ index ] )

			if !next {
				break
			}
		}
	}

}

// Functor

extension NSArray : Functor {

	public func map( @noescape transform: (AnyObject) -> AnyObject ) -> NSArray {
		var array = [AnyObject]()
		self.foreach { (value) in
			array.append( transform( value ) )
		}

		return array
	}

}

public func <^>( array: NSArray, @noescape transform: (AnyObject) -> AnyObject ) -> NSArray {
	return array.map( transform )
}

// Applicative

extension NSArray : ApplicativeSpecifics {

	public static func apply( transform: NSArray, value: NSArray ) -> NSArray {
		var values = [AnyObject]()
		let count = min( transform.count, value.count )
		for index in 0 ..< count {
			if let function = transform[ index ] as? (AnyObject) -> AnyObject {
				values.append( function( value[ index ] ) )
			}
		}

		return values
	}

}

public func <*>( transform: NSArray, value: NSArray ) -> NSArray {
	return NSArray.apply( transform, value: value )
}

// Monad

extension NSArray : MonadSpecifics {

	typealias MappedType = AnyObject


	// monad bind, >>*
	public func fmap( @noescape transform: (AnyObject) -> NSArray ) -> NSArray {
		return NSArray.flatten( self.map( transform ) )
	}

	// flatten
	public static func flatten( value: NSArray ) -> NSArray {
		var array = [AnyObject]()
		for element in value {
			switch element {
				case let element as NSArray:
					array.extend( element )

				default:
					array.append( element )
			}
		}

		return array
	}

}

public func >>*( value: NSArray, @noescape transform: (AnyObject) -> NSArray ) -> NSArray {
	return value.fmap( transform )
}

public func *<<( @noescape transform: (AnyObject) -> NSArray, value: NSArray ) -> NSArray {
	return value.fmap( transform )
}
