//
//  author:       fabrice truillot de chambrier
//  created:      19.01.2015
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


// References:
// https://www.haskell.org/haskellwiki/Typeclassopedia
// https://www.haskell.org/wikiupload/8/85/TMR-Issue13.pdf
// https://themonadreader.wordpress.com/


// MARK: Pointed

// PointedSpecifics

public protocol PointedSpecifics {

	typealias ValueType


	static func pure( value: ValueType ) -> Self

	init( ValueType ) // Haskell's pure

}

// Pointed

public protocol Pointed : Functor, PointedSpecifics {}


// MARK: -
// MARK: internal Pointed

// PointedSpecifics

protocol PointedSpecifics_ {

	typealias ValueType


	static func pure( value: ValueType ) -> Self

	init( ValueType ) // Haskell's pure

}

// Pointed

protocol Pointed_ : Functor_, PointedSpecifics_ {}
