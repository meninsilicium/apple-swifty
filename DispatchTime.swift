//
//  author:       fabrice truillot de chambrier
//  created:      23.03.2015
//
//  license:      see license.txt
//
//  © 2015-2015, men in silicium sàrl
//


import Foundation
import Dispatch


public struct DispatchTime {

	private var timestamp: Double
	private var timedelta: Double


	// MARK: initialization

	public init() {
		// now
		self.timestamp = 0
		self.timedelta = 0
	}

	public init( at: NSDate ) {
		// absolute
		self.timestamp = at.timeIntervalSinceReferenceDate
		self.timedelta = 0
	}

	public init( after: Double ) {
		// relative
		self.timestamp = 0
		self.timedelta = after
	}

	public var time: dispatch_time_t {
		// now
		if self.timestamp == 0 && self.timedelta == 0 {
			return dispatch_time( DISPATCH_TIME_NOW, 0 )
		}

		// absolute
		if self.timestamp > 0 && self.timedelta == 0 {
			let nanoseconds = round( (self.timestamp - NSDate.timeIntervalSinceReferenceDate()) * 1e9 )
			return dispatch_time( DISPATCH_TIME_NOW, Int64( nanoseconds ) )
		}

		// relative
		if self.timestamp == 0 && self.timedelta > 0 {
			let nanoseconds = round( self.timedelta * 1e9 )
			return dispatch_time( DISPATCH_TIME_NOW, Int64( nanoseconds ) )
		}

		// mixed
		if self.timestamp > 0 && self.timedelta > 0 {
			let nanoseconds = round( (self.timestamp - NSDate.timeIntervalSinceReferenceDate() + self.timedelta) * 1e9 )
			return dispatch_time( DISPATCH_TIME_NOW, Int64( nanoseconds ) )
		}

		// fallback
		return dispatch_time( DISPATCH_TIME_NOW, 0 )
	}

	public var date: NSDate {
		// now
		if self.timestamp == 0 && self.timedelta == 0 {
			return NSDate()
		}

		// absolute
		if self.timestamp > 0 && self.timedelta == 0 {
			return NSDate( timeIntervalSinceReferenceDate: self.timestamp )
		}

		// relative
		if self.timestamp == 0 && self.timedelta > 0 {
			return NSDate( timeIntervalSinceNow: self.timedelta )
		}

		// mixed
		if self.timestamp > 0 && self.timedelta > 0 {
			let seconds = self.timestamp - NSDate.timeIntervalSinceReferenceDate() + self.timedelta
			return NSDate( timeIntervalSinceReferenceDate: seconds )
		}

		// fallback
		return NSDate()
	}

}

extension DispatchTime {

	internal func isNow() -> Bool {
		return self.timestamp == 0 && self.timedelta == 0
	}

	internal func isAbsolute() -> Bool {
		return self.timestamp > 0 && self.timedelta == 0
	}

	internal func isRelative() -> Bool {
		return self.timestamp == 0 && self.timedelta > 0
	}

}

// MARK: Equatable

extension DispatchTime : Equatable {}

public func ==( lhs: DispatchTime, rhs: DispatchTime ) -> Bool {
	if lhs.timestamp == rhs.timestamp && lhs.timedelta == rhs.timedelta { return true }
	if lhs.isNow() && rhs.isNow() { return true }

	return lhs.time == lhs.time
}
