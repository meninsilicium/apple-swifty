//
//  author:       fabrice truillot de chambrier
//  created:      14.02.2015
//
//  license:      see license.txt
//
//  © 2015-2015, men in silicium sàrl
//


import class Foundation.NSDictionary


extension Dictionary {

	/// updateValue(_, value:)
	mutating func updateValue( key: Key, value: Value ) -> Value? {
		return self.updateValue( value, forKey: key )
	}

	mutating func updateValues( values: Dictionary ) -> Dictionary {
		var dictionary = [Key: Value]()
		for (key, value) in values {
			let oldValue = self.updateValue( key, value: value )
			if oldValue != nil {
				dictionary[ key ] = oldValue
			}
		}

		return dictionary
	}

	// naive first implementation
	func union( rhs: Dictionary ) -> Dictionary {
		var values = self
		for (let key, let value) in rhs {
			if values[ key ] == nil {
				values[ key ] = value
			}
		}

		return values
	}

	// naive first implementation
	func subtract( rhs: Dictionary ) -> Dictionary {
		var values = [Key: Value]()
		for (key, value) in self {
			if rhs[ key ] == nil {
				values[ key ] = value
			}
		}

		return values
	}

	// naive first implementation
	func intersect( rhs: Dictionary ) -> Dictionary {
		var values = [Key: Value]()
		for (key, value) in self {
			if rhs[ key ] != nil {
				values[ key ] = value
			}
		}

		return values
	}

}

// Iterable

extension Dictionary : Iterable_ {

	/// apply the function to each (key, value) pairs
	func foreach( @noescape function: (Key, Value) -> Void ) {
		for (key, value) in self {
			function( key, value )
		}
	}

	/// apply the function to each (key, value) pairs
	func foreach( @noescape function: (Int, (Key, Value)) -> Void ) {
		for (index, (key, value)) in Swift.enumerate( self ) {
			function( index, (key, value) )
		}
	}

}

// Enumerable

extension Dictionary : Enumerable_ {

	func enumerate( @noescape function: (Int, (Key, Value)) -> Bool ) {
		for (index, (key, value)) in Swift.enumerate( self ) {
			let next = function( index, (key, value) )
			if !next { break }
		}
	}
	
}
