//
//  author:       fabrice truillot de chambrier
//  created:      26.07.2014
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


// MARK: unwrap

/// upwrap takes one or several optionals, check for values and return a populated tuple or nil if any value is missing
@availability( *, deprecated=1.2 )
func unwrap<T>( value: T? ) -> (T)? {
	switch (value) {
		case (.Some):
			return (value!)

		default:
			return nil
	}
}

/// upwrap takes one or several optionals, check for values and return a populated tuple or nil if any value is missing
@availability( *, deprecated=1.2 )
func unwrap<T1, T2>( value1: T1?, value2: T2? ) -> (T1, T2)? {
	switch (value1, value2) {
		case (.Some, .Some):
			return (value1!, value2!)

		default:
			return nil
	}
}

/// upwrap takes one or several optionals, check for values and return a populated tuple or nil if any value is missing
@availability( *, deprecated=1.2 )
func unwrap<T1, T2, T3>( value1: T1?, value2: T2?, value3: T3? ) -> (T1, T2, T3)? {
	switch (value1, value2, value3) {
		case (.Some, .Some, .Some):
			return (value1!, value2!, value3!)

		default:
			return nil
	}
}

/// upwrap takes one or several optionals, check for values and return a populated tuple or nil if any value is missing
@availability( *, deprecated=1.2 )
func unwrap<T1, T2, T3, T4>( value1: T1?, value2: T2?, value3: T3?, value4: T4? ) -> (T1, T2, T3, T4)? {
	switch (value1, value2, value3, value4) {
		case (.Some, .Some, .Some, .Some):
			return (value1!, value2!, value3!, value4!)

		default:
			return nil
	}
}

/// upwrap takes one or several optionals, check for values and return a populated tuple or nil if any value is missing
@availability( *, deprecated=1.2 )
func unwrap<T1, T2, T3, T4, T5>( value1: T1?, value2: T2?, value3: T3?, value4: T4?, value5: T5? ) -> (T1, T2, T3, T4, T5)? {
	switch (value1, value2, value3, value4, value5) {
		case (.Some, .Some, .Some, .Some, .Some):
			return (value1!, value2!, value3!, value4!, value5!)

		default:
			return nil
	}
}

/// upwrap takes one or several optionals, check for values and return a populated tuple or nil if any value is missing
@availability( *, deprecated=1.2 )
func unwrap<T1, T2, T3, T4, T5, T6>( value1: T1?, value2: T2?, value3: T3?, value4: T4?, value5: T5?, value6: T6? ) -> (T1, T2, T3, T4, T5, T6)? {
	switch (value1, value2, value3, value4, value5, value6) {
		case (.Some, .Some, .Some, .Some, .Some, .Some):
			return (value1!, value2!, value3!, value4!, value5!, value6!)

		default:
			return nil
	}
}

// MARK: unwrap with a function

/// upwrap takes one or several optionals and calls the supplied function if all the values exist
@availability( *, deprecated=1.2 )
func unwrap<T>( value: T?, function: (T) -> Void ) -> Void {
	switch (value) {
		case (.Some):
			function( value! )

		default:
			break
	}
}

/// upwrap takes one or several optionals and calls the supplied function if all the values exist
@availability( *, deprecated=1.2 )
func unwrap<T1, T2>( value1: T1?, value2: T2?, function: (T1, T2) -> Void ) -> Void {
	switch (value1, value2) {
		case (.Some, .Some):
			function( value1!, value2! )

		default:
			break
	}
}

/// upwrap takes one or several optionals and calls the supplied function if all the values exist
@availability( *, deprecated=1.2 )
func unwrap<T1, T2, T3>( value1: T1?, value2: T2?, value3: T3?, function: (T1, T2, T3) -> Void ) -> Void {
	switch (value1, value2, value3) {
		case (.Some, .Some, .Some):
			function( value1!, value2!, value3! )

		default:
			break
	}
}

/// upwrap takes one or several optionals and calls the supplied function if all the values exist
@availability( *, deprecated=1.2 )
func unwrap<T1, T2, T3, T4>( value1: T1?, value2: T2?, value3: T3?, value4: T4?, function: (T1, T2, T3, T4) -> Void ) -> Void {
	switch (value1, value2, value3, value4) {
		case (.Some, .Some, .Some, .Some):
			function( value1!, value2!, value3!, value4! )

		default:
			break
	}
}

/// upwrap takes one or several optionals and calls the supplied function if all the values exist
@availability( *, deprecated=1.2 )
func unwrap<T1, T2, T3, T4, T5>( value1: T1?, value2: T2?, value3: T3?, value4: T4?, value5: T5?, function: (T1, T2, T3, T4, T5) -> Void ) -> Void {
	switch (value1, value2, value3, value4, value5) {
		case (.Some, .Some, .Some, .Some, .Some):
			function( value1!, value2!, value3!, value4!, value5! )

		default:
			break
	}
}

/// upwrap takes one or several optionals and calls the supplied function if all the values exist
@availability( *, deprecated=1.2 )
func unwrap<T1, T2, T3, T4, T5, T6>( value1: T1?, value2: T2?, value3: T3?, value4: T4?, value5: T5?, value6: T6?, function: (T1, T2, T3, T4, T5, T6) -> Void ) -> Void {
	switch (value1, value2, value3, value4, value5, value6) {
		case (.Some, .Some, .Some, .Some, .Some, .Some):
			function( value1!, value2!, value3!, value4!, value5!, value6! )

		default:
			break
	}
}

// MARK: - curry

/// curry transforms a n-arity function in a 1-arity function which return a 1-arity function which... until the argument list is depleted.
/// The function f(a, b, c...) -> r becomes f(a)(b)(c)... -> r or f(a) -> (b) -> (c) ... -> r
func curry<T, R>( function: (T) -> R ) -> (T) -> R {
	return function
}

/// curry transforms a n-arity function in a 1-arity function which return a 1-arity function which... until the argument list is depleted.
/// The function f(a, b, c...) -> r becomes f(a)(b)(c)... -> r or f(a) -> (b) -> (c) ... -> r
func curry<T1, T2, R>( function: (T1, T2) -> R ) -> (T1) -> (T2) -> R {
	return { (t1) in { (t2) in function( t1, t2 ) } }
}

/// curry transforms a n-arity function in a 1-arity function which return a 1-arity function which... until the argument list is depleted.
/// The function f(a, b, c...) -> r becomes f(a)(b)(c)... -> r or f(a) -> (b) -> (c) ... -> r
func curry<T1, T2, T3, R>( function: (T1, T2, T3) -> R ) -> (T1) -> (T2) -> (T3) -> R {
	return { (t1) in { (t2) in { (t3) in function( t1, t2, t3 ) } } }
}

/// curry transforms a n-arity function in a 1-arity function which return a 1-arity function which... until the argument list is depleted.
/// The function f(a, b, c...) -> r becomes f(a)(b)(c)... -> r or f(a) -> (b) -> (c) ... -> r
func curry<T1, T2, T3, T4, R>( function: (T1, T2, T3, T4) -> R ) -> (T1) -> (T2) -> (T3) -> (T4) -> R {
	return { (t1) in { (t2) in { (t3) in { (t4) in function( t1, t2, t3, t4 ) } } } }
}

/// curry transforms a n-arity function in a 1-arity function which return a 1-arity function which... until the argument list is depleted.
/// The function f(a, b, c...) -> r becomes f(a)(b)(c)... -> r or f(a) -> (b) -> (c) ... -> r
func curry<T1, T2, T3, T4, T5, R>( function: (T1, T2, T3, T4, T5) -> R ) -> (T1) -> (T2) -> (T3) -> (T4) -> (T5) -> R {
	return { (t1) in { (t2) in { (t3) in { (t4) in { (t5) in function( t1, t2, t3, t4, t5 ) } } } } }
}

/// curry transforms a n-arity function in a 1-arity function which return a 1-arity function which... until the argument list is depleted.
/// The function f(a, b, c...) -> r becomes f(a)(b)(c)... -> r or f(a) -> (b) -> (c) ... -> r
func curry<T1, T2, T3, T4, T5, T6, R>( function: (T1, T2, T3, T4, T5, T6) -> R ) -> (T1) -> (T2) -> (T3) -> (T4) -> (T5) -> (T6) -> R {
	return { (t1) in { (t2) in { (t3) in { (t4) in { (t5) in { (t6) in function( t1, t2, t3, t4, t5, t6 ) } } } } } }
}

// MARK: - uncurry

/// uncurry transforms a curried 1-arity function chain in a, well, n-arity function
func uncurry<T, R>( function: (T) -> R ) -> (T) -> R {
	return function
}

/// uncurry transforms a curried 1-arity function chain in a, well, n-arity function
func uncurry<T1, T2, R>( function: (T1) -> (T2) -> R ) -> (T1, T2) -> R {
	return { (t1, t2) in function(t1)(t2) }
}

/// uncurry transforms a curried 1-arity function chain in a, well, n-arity function
func uncurry<T1, T2, T3, R>( function: (T1) -> (T2) -> (T3) -> R ) -> (T1, T2, T3) -> R {
	return { (t1, t2, t3) in function(t1)(t2)(t3) }
}

/// uncurry transforms a curried 1-arity function chain in a, well, n-arity function
func uncurry<T1, T2, T3, T4, R>( function: (T1) -> (T2) -> (T3) -> (T4) -> R ) -> (T1, T2, T3, T4) -> R {
	return { (t1, t2, t3, t4) in function(t1)(t2)(t3)(t4) }
}

/// uncurry transforms a curried 1-arity function chain in a, well, n-arity function
func uncurry<T1, T2, T3, T4, T5, R>( function: (T1) -> (T2) -> (T3) -> (T4) -> (T5) -> R ) -> (T1, T2, T3, T4, T5) -> R {
	return { (t1, t2, t3, t4, t5) in function(t1)(t2)(t3)(t4)(t5) }
}

/// uncurry transforms a curried 1-arity function chain in a, well, n-arity function
func uncurry<T1, T2, T3, T4, T5, T6, R>( function: (T1) -> (T2) -> (T3) -> (T4) -> (T5) -> (T6) -> R ) -> (T1, T2, T3, T4, T5, T6) -> R {
	return { (t1, t2, t3, t4, t5, t6) in function(t1)(t2)(t3)(t4)(t5)(t6) }
}

// MARK: - hasValue

/// universal value existence test function
func hasValue<T>( value: T? ) -> Bool {
	return value != nil
}

/// universal value existence test function
func hasValue( value: String ) -> Bool {
	return !value.isEmpty
}

/// universal value existence test function
func hasValue<V>( value: [V] ) -> Bool {
	return !value.isEmpty
}

/// universal value existence test function
func hasValue<K: Hashable, V>( value: [K: V] ) -> Bool {
	return !value.isEmpty
}

// MARK: - isEmpty

/// universal emptiness test function
func isEmpty<T>( value: T? ) -> Bool {
	return value == nil
}

/// universal emptiness test function
func isEmpty( value: String ) -> Bool {
	return value.isEmpty
}

/// universal emptiness test function
func isEmpty<V>( value: [V] ) -> Bool {
	return value.isEmpty
}

/// universal emptiness test function
func isEmpty<K: Hashable, V>( value: [K: V] ) -> Bool {
	return value.isEmpty
}

// MARK: - union

func union<S1: SequenceType, S2: SequenceType where S1.Generator.Element == S2.Generator.Element, S1.Generator.Element: Equatable, S2.Generator.Element: Equatable>( lhs: S1, rhs: S2 ) -> [S1.Generator.Element] {
	var values = Array<S1.Generator.Element>( lhs )
	values.extend( rhs )

	return values
}

func union<T: Equatable>( lhs: Set<T>, rhs: Set<T> ) -> Set<T> {
	return lhs.union( rhs )
}

func union<T: Equatable>( lhs: [T], rhs: [T] ) -> [T] {
	var values = lhs
	values.extend( rhs )

	return values
}

func union<K: Hashable, T: Equatable>( lhs: [K: T], rhs: [K: T] ) -> [K: T] {
	var values = lhs
	for (let key, let value) in rhs {
		if values[ key ] == nil {
			values[ key ] = value
		}
	}

	return values
}

// MARK: - subtract

func subtract<T: Equatable>( lhs: Set<T>, rhs: Set<T> ) -> Set<T> {
	return lhs.subtract( rhs )
}

func subtract<T: Equatable>( lhs: [T], rhs: [T] ) -> [T] {
	var values = [T]()
	for element in lhs {
		if rhs !∋ element {
			values.append( element )
		}
	}

	return values
}

// MARK: - intersect

func intersect<T: Equatable>( lhs: Set<T>, rhs: Set<T> ) -> Set<T> {
	return lhs.intersect( rhs )
}

func intersect<T: Equatable>( lhs: [T], rhs: [T] ) -> [T] {
	var values = [T]()
	for element in lhs {
		if (rhs ∋ element) && (values !∋ element) {
			values.append( element )
		}
	}
	for element in rhs {
		if (lhs ∋ element) && (values !∋ element) {
			values.append( element )
		}
	}

	return values
}

// MARK: - exclusiveOr

func exclusiveOr<T: Equatable>( lhs: Set<T>, rhs: Set<T> ) -> Set<T> {
	return lhs.exclusiveOr( rhs )
}

func exclusiveOr<T: Equatable>( lhs: [T], rhs: [T] ) -> [T] {
	var values = [T]()
	for element in lhs {
		if rhs !∋ element {
			values.append( element )
		}
	}
	for element in rhs {
		if lhs !∋ element {
			values.append( element )
		}
	}

	return values
}

// MARK: - tap

func tap<T>( var value: T, function: (inout T) -> Void ) -> T {
	function( &value )

	return value
}

// MARK: - memoize

func memoize<T: Hashable, R>( function: (((T) -> R), T) -> R) -> ((T) -> R) {
	var memoized: ((T) -> R)? = nil
	var cache = [T: R]()

	memoized = { (value: T) -> R in
		if let cached = cache[ value ] {
			return cached
		}
		else {
			let cached = function( memoized!, value )
			cache[ value ] = cached
			return cached
		}
	}

	return memoized!
}
