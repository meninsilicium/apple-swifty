//
//  author:       fabrice truillot de chambrier
//  created:      29.01.2015
//
//  license:      see license.txt
//
//  © 2015-2015, men in silicium sàrl
//


// MARK: JSONValidation

public struct JSONValidation {

	public private(set) var validators: Set<JSONValidator>

	// Freezable

	public private(set) var isFrozen: Bool = false

}

// MARK: types

public typealias JSONValidationFunction = (validation: JSONValidation, value: JSONValue) -> Bool


// MARK: initialization

extension JSONValidation {

	public init() {
		self.validators = Set()
	}

}

// MARK: subscripts

extension JSONValidation {

	/// subscript with a JSONPath
	private subscript( path: JSONPath ) -> JSONValidator? {
		// [ path: JSONPath ] -> JSONValidator?
		get {
			var validator: JSONValidator? = nil
			self.validators.enumerate { (index, ivalidator) -> Bool in
				if ivalidator.path == path {
					validator = ivalidator
					return false
				}

				return true
			}

			return validator
		}

		// [ path: JSONPath ] = JSONValidator
		set( validator ) {
			if self.isFrozen { return }

			let lvalidator = JSONValidator( path: path )
			if let validator = validator {
				self.validators.remove( lvalidator )
				self.validators.insert( validator )
			}
			else {
				self.validators.remove( lvalidator )
			}
		}
	}

}

extension JSONValidation {

	public mutating func assess( path: JSONPath, optional: Bool = true, function: JSONValidationFunction ) {
		self.insert( path, optional: optional, function: function )
	}

	public mutating func insert( path: JSONPath, optional: Bool = true, function: JSONValidationFunction ) {
		if self.isFrozen { return }

		var validator = JSONValidator( path: path )
		validator.optional = optional
		validator.function = function

		self.validators.insert( validator )
	}

	public mutating func remove( path: JSONPath ) -> JSONValidator? {
		if self.isFrozen { return nil }

		var validator = JSONValidator( path: path )

		return self.validators.remove( validator )
	}

}

// MARK: Freezable

extension JSONValidation : Freezable {

	public mutating func freeze() -> Bool {
		if self.isFrozen { return true }

		self.isFrozen = true

		return false
	}

}

// MARK: validation

extension JSONValidation {

	public func validate( json: JSONValue ) -> Bool {
		var valid: Bool = true

		self.validators.foreach { (validator: JSONValidator) in
			let ivalid = validator.validate( self, json: json )

			valid = valid && ivalid
		}

		return valid
	}

}

extension JSONValue {

	public func validate( validation: JSONValidation ) -> Bool {
		return validation.validate( self )
	}

}

// MARK: -
// MARK: JSONValidator

public struct JSONValidator : Hashable {

	public private(set) var path: JSONPath

	public private(set) var optional: Bool = true
	private var function: JSONValidationFunction?

}

// MARK: initialization

extension JSONValidator {

	init() {
		self.path = JSONPath()
		self.function = nil
	}

	public init( path: JSONPath ) {
		self.path = path
		self.function = nil
	}

	public init( path: JSONPath, optional: Bool ) {
		self.path = path
		self.optional = optional
		self.function = nil
	}

}

// MARK: Equatable

extension JSONValidator : Equatable {}

public func ==( lhs: JSONValidator, rhs: JSONValidator ) -> Bool {
	return lhs.path == rhs.path
}

// MARK: Hashable

extension JSONValidator : Hashable {

	public var hashValue: Int {
		return self.path.hashValue
	}

}

// MARK: validation

extension JSONValidator {

	func validate( validation: JSONValidation, json: JSONValue ) -> Bool {
		if let subjson = json[ self.path ] {
			if let function = self.function {
				let valid = function( validation: validation, value: subjson )

				return valid
			}

			return true
		}

		return self.optional
	}

}
