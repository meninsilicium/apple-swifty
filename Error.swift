//
//  author:       fabrice truillot de chambrier
//  created:      22.01.2015
//
//  license:      see license.txt
//
//  © 2015-2015, men in silicium sàrl
//


import class Foundation.NSDate
import class Foundation.NSError
import class Foundation.NSThread


public struct Error {

	public init( name: String, domain: String, code: Int = 0 ) {
		self.name = name
		self.domain = domain
		self.code = code

		self.timestamp = NSDate.timeIntervalSinceReferenceDate()
		self.location = nil

		self.attributes = nil
	}

	public init( error: NSError ) {
		self.name = "NSError"
		self.domain = error.domain
		self.code = error.code

		self.timestamp = NSDate.timeIntervalSinceReferenceDate()

		self.location = nil

		var attributes = [String: AnyObject]()
		for (key, value) in (error.userInfo as! [String: AnyObject]) {
			attributes[ key ] = value
		}
		attributes[ "Stack" ] = NSThread.callStackSymbols() as! [String]
		self.attributes = attributes
	}


	public let name: String

	public let domain: String
	public let code: Int

	public let timestamp: Double
	public let location: String?

	public let attributes: [String: AnyObject]?

	public var error: NSError {
		return NSError( domain: self.domain, code: Int( self.code ),
			userInfo: [ "Error.name": self.name, "Error.timestamp": self.timestamp, "Error.location": self.location ?? "" ] );
	}

}

extension Error : Printable {

	public var description: String {
		return "Error \( self.name ): \( self.domain ):\( toString( self.code ) )"
	}

}
