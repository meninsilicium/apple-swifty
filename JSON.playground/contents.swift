//: # Swifty.JSON

import Swift
import Swifty

import Foundation
import Darwin


//: # JSON examples

//: A json as a string

let json_string = "{ \"coordinate\" : { \"latitude\" : 46.2050295, \"longitude\" : 6.1440885, \"city\" : \"Geneva\" }, \"source\" : \"string\" }"


//: ## Initialization

//: with a raw string or NSData

var json: JSONValue = [:]
if let maybe_json = JSON.decode( json_string ).value {
	json = maybe_json
}

json.prettyprint()


//: with literals

json = [
	"coordinate" : [
		"latitude" : 46.2050295,
		"longitude" : 6.144,
		"city" : "Geneva"
	],
	"source" : "literals"
]
json.prettyprint()


//: ## Read

//: with direct access

var json_longitude = json[ "coordinate", "longitude" ]?.double ?? 0
var json_latitude = json[ "coordinate", "latitude" ]?.double ?? 0


//: with a path

var json_path: JSONPath = [ "coordinate", "latitude" ]

var json_latitude_number = json[ json_path ]?.number ?? 0
json_latitude = json[ json_path ]?.double ?? 0

var json_city = json[ JSONPath( "coordinate", "city" ) ]?.string ?? ""


//: ## Validation

var json_validation = JSONValidation()

// assess [ "coordinate", "latitude" ]
json_validation.assess( json_path, optional: false ) { (validation, json) -> Bool in
	switch json {
		case .JSONNumber, .JSONDouble, .JSONInteger:
			let value = json.double
			return (value >= -180) || (value <= 180)

		default:
			return false
	}
}

// assess [ "coordinate", "longitude" ]
json_validation.assess( [ "coordinate", "longitude" ], optional: true ) { (validation, json) -> Bool in
	switch json {
		case .JSONNumber, .JSONDouble, .JSONInteger:
			let value = json.double
			return (value >= -180) || (value <= 180)

		default:
			return false
	}
}

//: Validate

let valid = json.validate( json_validation )
json_validation.validate( json )


//----------------------------------------------------------------
