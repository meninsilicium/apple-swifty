//
//  author:       fabrice truillot de chambrier
//  created:      05.08.2014
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


/// A simple box for a value
/// - needed to work around some compiler limitations (as of 2014-08)
/// - allows to pass a value by reference, in a box

public final class Box<T> {

	private let value: T

	public init( _ value: T ) {
		self.value = value
	}

	deinit {
	}

	public func unbox() -> T {
		return self.value
	}

}

// Iterable

extension Box : Iterable {

	public func foreach( @noescape function: (T) -> Void ) {
		function( self.value )
	}

	public func foreach( @noescape function: (Int, T) -> Void ) {
		function( 0, self.value )
	}

}

// Equatable

//extension Box : Equatable {}
// A Box<T> is Equatable only and only if T is Equatable

public func ==<T: Equatable>( lhs: Box<T>, rhs: Box<T> ) -> Bool {
	return lhs.value == rhs.value
}

public func !=<T: Equatable>( lhs: Box<T>, rhs: Box<T> ) -> Bool {
	return lhs.value != rhs.value
}

// Functor

extension Box : Functor {

	typealias MappedType = Any
	typealias MappedFunctorType = Box<MappedType>


	public func map<R>( @noescape function: (T) -> R ) -> Box<R> {
		return Box<R>( function( self.value ) )
	}

}

public func <^><T, R>( value: Box<T>, @noescape transform: (T) -> R ) -> Box<R> {
	return value.map( transform )
}

extension Box : Pointed {

	public static func pure( value: T ) -> Box<T> {
		return Box<T>( value )
	}

}

extension Box : Applicative {

	typealias ApplicativeType = Box<(T) -> MappedType>


	public static func apply<R>( transform: Box<(T) -> R>, value: Box<T> ) -> Box<R> {
		return Box<R>( transform.value( value.unbox() ) )
	}

}

/*
func <*><T, R>( transform: Box<(T) -> R>, value: Box<T> ) -> Box<R> {
	return Box.apply( transform, value: value )
}
*/

extension Box : Monad {

	/// flat map
	public func fmap<R>( @noescape transform: (T) -> Box<R> ) -> Box<R> {
		return Box<R>.flatten( self.map( transform ) )
	}

	public static func flatten( value: Box<Box<T>> ) -> Box<T> {
		return value.value
	}

}

/*
public func >>*<T, R>( value: Box<T>, transform: (T) -> Box<R> ) -> Box<R> {
	return value.fmap( transform )
}
*/


// Printable

extension Box : Printable {

	public var description: String {
		return "Box( \(toString( self.value )) )"
	}

}
