//
//  author:       fabrice truillot de chambrier
//  created:      29.01.2015
//
//  license:      see license.txt
//
//  © 2015-2015, men in silicium sàrl
//


// MARK: JSONPath

public struct JSONPath {

	private var components: [JSONPathComponent]

}

// MARK: JSONPathComponent

public enum JSONPathComponent {

	case Key( String )
	case Index( Int )

}

// MARK: conforming types

public protocol JSONPathComponentType {}

extension String : JSONPathComponentType {}
extension Int : JSONPathComponentType {}


// MARK: -
// MARK: implementation

// MARK: JSONPath init

extension JSONPath {

	public init() {
		self.components = []
	}

	// init with 1 component
	public init( _ any: JSONPathComponentType ) {
		switch any {
			case let key as String:
				self.init( JSONPathComponent( key: key ) )

			case let index as Int:
				self.init( JSONPathComponent( index: index ) )

			default:
				self.init()
				println( "JSONPath.init didn't match \( any )" )
		}
	}

	// init with 1+ component
	public init( _ component: JSONPathComponentType, _ components: JSONPathComponentType... ) {
		// assembling an array
		var array = [JSONPathComponentType]()
		array.append( component )
		array.extend( components )

		self.init( array )
	}

	public init( _ components: [JSONPathComponentType] ) {
		var array = [JSONPathComponent]()
		for component in components {
			switch component {
				case let key as String:
					array.append( JSONPathComponent( key: key ) )

				case let index as Int:
					array.append( JSONPathComponent( index: index ) )

				default:
					println( "JSONPath.init didn't match \(component)" )
					break
			}
		}

		self.components = array
	}

	public init( _ key: String ) {
		self.components = [ JSONPathComponent( key: key ) ]
	}

	public init( _ index: Int ) {
		self.components = [ JSONPathComponent( index: index ) ]
	}

	private init( _ component: JSONPathComponent ) {
		self.components = [ component ]
	}

	private init( _ components: [JSONPathComponent] ) {
		self.components = components
	}

}

// MARK: JSONPath init with literals

// Array

extension JSONPath : ArrayLiteralConvertible {

	public typealias Element = JSONPathComponentType

	public init( arrayLiteral elements: JSONPathComponentType... ) {
		var array = [JSONPathComponent]()
		for element in elements {
			switch element {
				case let key as String:
					array.append( JSONPathComponent( key: key ) )

				case let index as Int:
					array.append( JSONPathComponent( index: index ) )

				default:
					break
			}
		}

		self.init( array )
	}

}

// String

extension JSONPath : StringLiteralConvertible {

	public init( stringLiteral value: String ) {
		self.init( value )
	}

	public init( extendedGraphemeClusterLiteral value: StaticString ) {
		self.init( value.stringValue )
	}

	public init( unicodeScalarLiteral value: Character ) {
		self.init( String( value ) )
	}

}

// Int

extension JSONPath : IntegerLiteralConvertible {

	public init( integerLiteral value: Int ) {
		self.init( value )
	}

}

// MARK: JSONPath accessors

extension JSONPath {

	public var count: Int {
		return self.components.count
	}

	public var isEmpty: Bool {
		return self.components.isEmpty
	}

	public var first: JSONPathComponent? {
		return self.components.first
	}

	public func take( count: Int ) -> JSONPath {
		if count <= 0 {
			return JSONPath()
		}

		if count <= self.count {
			let splice = self.components[ 0 ..< count ]
			let array = [JSONPathComponent]( splice )

			return JSONPath( array )
		}

		return self
	}

	public func drop( count: Int ) -> JSONPath {
		if count <= 0 {
			return self
		}

		if count <= self.count {
			let splice = self.components[ count ..< self.count ]
			let array = [JSONPathComponent]( splice )

			return JSONPath( array )
		}

		return JSONPath()
	}

	public func dropFirst() -> JSONPath {
		let slice = Swift.dropFirst( self.components )
		let array = [JSONPathComponent]( slice )
		let path = JSONPath( array )

		return path
	}

	var body: JSONPath {
		let slice = Swift.dropLast( self.components )
		let array = [JSONPathComponent]( slice )
		let path = JSONPath( array )

		return path
	}

	var tail: JSONPath {
		let slice = Swift.dropFirst( self.components )
		let array = [JSONPathComponent]( slice )
		let path = JSONPath( array )

		return path
	}

	public func dropLast() -> JSONPath {
		let slice = Swift.dropLast( self.components )
		let array = [JSONPathComponent]( slice )
		let path = JSONPath( array )

		return path
	}

	public var last: JSONPathComponent? {
		return self.components.last
	}

}

// MARK: JSONPath : Equatable

extension JSONPath : Equatable {}

public func ==( lhs: JSONPath, rhs: JSONPath ) -> Bool {
	if lhs.components.count != rhs.components.count {
		return false
	}

	for index in 0 ..< lhs.components.count {
		let lcomponent = lhs.components[ index ]
		let rcomponent = rhs.components[ index ]

		if lcomponent != rcomponent {
			return false
		}
	}

	return true
}

// MARK: JSONPath : Comparable

extension JSONPath : Comparable {}

public func <( lhs: JSONPath, rhs: JSONPath ) -> Bool {
	let lcomponent = lhs.first
	let rcomponent = rhs.first

	switch (lcomponent, rcomponent) {
		case (.Some, .Some):
			if lcomponent < rcomponent { return true }
			if lcomponent > rcomponent { return false }

			return lhs.body < rhs.body

		case (.None, .Some):
			return true

		case (.Some, .None), (.None, .None):
			return false
	}
}

extension JSONPathComponent : Comparable {}

public func <( lhs: JSONPathComponent, rhs: JSONPathComponent ) -> Bool {
	switch (lhs, rhs) {
		case (.Key( let lkey ), .Key( let rkey )):
			return lkey < rkey

		case (.Index( let lindex ), .Index( let rindex )):
			return lindex < rindex

		case (.Key, .Index):
			return true

		case (.Index, .Key):
			return false
	}
}

extension JSONPath {

	public func isPrefixOf( path: JSONPath ) -> Bool {
		if self.count <= path.count {
			for index in 0 ..< self.count {
				if self[ index ] != path[ index ] {
					return false
				}
			}

			return true
		}
		else {
			return false
		}
	}
}

// MARK: JSONPath : Hashable

extension JSONPath : Hashable {

	public var hashValue: Int {
		var path = join( "+", self.components.map { $0.string } )

		return path.hashValue
	}
	
}

// MARK: JSONPath : SequenceType

extension JSONPath : SequenceType {

	typealias Generator = IndexingGenerator<[JSONPathComponent]>

	public func generate() -> IndexingGenerator<[JSONPathComponent]> {
		return self.components.generate()
	}

}

// MARK: JSONPath : CollectionType

extension JSONPath : CollectionType {

	public typealias Index = Int

	public var startIndex: Int {
		return self.components.startIndex
	}

	public var endIndex: Int {
		return self.components.endIndex
	}

	public subscript( position: Int ) -> JSONPathComponent {
		return self.components[ position ]
	}

}

// MARK: JSONPath : ExtensibleCollectionType

extension JSONPath : ExtensibleCollectionType {

	public mutating func reserveCapacity( capacity: Int ) {
		self.components.reserveCapacity( capacity )
	}

	public mutating func append( component: JSONPathComponent ) {
		self.components.append( component )
	}

	public mutating func append( component: JSONPathComponentType ) {
		self.components.append( JSONPathComponent( component ) )
	}

	public mutating func extend<S : SequenceType where S.Generator.Element == JSONPathComponent>( elements: S ) {
		self.components.extend( elements )
	}

	public mutating func extend( elements: JSONPath ) {
		self.components.extend( elements.components )
	}

}

public func +=( inout lhs: JSONPath, rhs: JSONPathComponentType ) {
	lhs.append( rhs )
}

public func +=( inout lhs: JSONPath, rhs: JSONPathComponent ) {
	lhs.append( rhs )
}

public func +=( inout lhs: JSONPath, rhs: JSONPath ) {
	lhs.extend( rhs )
}

// MARK: JSONPath : Printable

extension JSONPath : Printable {

	public var description: String {
		return "[ " + join( ", ", self.components.map { $0.string } ) + " ]"
	}

}

// MARK: -
// MARK: JSONPathComponent init

extension JSONPathComponent {

	private init( key: String ) {
		self = .Key( key )
	}

	private init( index: Int ) {
		self = .Index( index )
	}

	// copy init
	private init( component: JSONPathComponent ) {
		self = component
	}

	private init( _ any: JSONPathComponentType ) {
		switch any {
			case let key as String:
				self.init( key: key )

			case let index as Int:
				self.init( index: index )

			default:
				self.init( key: "" )
				println( "JSONPathComponent.init didn't match \( any )" )
		}
	}

}

// MARK: JSONPathComponent init with literals

// String

extension JSONPathComponent : StringLiteralConvertible {

	public init( stringLiteral value: String ) {
		self.init( key: value )
	}

	public init( extendedGraphemeClusterLiteral value: StaticString ) {
		self.init( key: value.stringValue )
	}

	public init( unicodeScalarLiteral value: Character ) {
		self.init( key: String( value ) )
	}

}

// Int

extension JSONPathComponent : IntegerLiteralConvertible {

	public init( integerLiteral value: Int ) {
		self.init( index: value )
	}

}

// MARK: JSONPathComponent accessors

extension JSONPathComponent {

	var key: String? {
		switch self {
			case .Key( let key ):
				return key

			case .Index:
				return nil
		}
	}

	var index: Int? {
		switch self {
			case .Key:
				return nil

			case .Index( let index ):
				return index
		}
	}

	var string: String {
		switch self {
			case .Key( let key ):
				return key

			case .Index( let index ):
				return toString( index )
		}
	}

}

// MARK: JSONPathComponent : Equatable

extension JSONPathComponent : Equatable {}

public func ==( lhs: JSONPathComponent, rhs: JSONPathComponent ) -> Bool {
	switch (lhs, rhs) {
		case (.Key( let lkey ), .Key( let rkey ) ):
			return lkey == rkey

		case (.Index( let lindex ), .Index( let rindex )):
			return lindex == rindex

		default:
			return false
	}
}

// MARK: JSONPathComponent : Hashable

extension JSONPathComponent : Hashable {

	public var hashValue: Int {
		switch self {
			case .Key( let key ):
				return key.hashValue

			case .Index( let index ):
				return index.hashValue
		}
	}

}

// MARK: JSONPathComponent : Printable

extension JSONPathComponent : Printable {

	public var description: String {
		switch self {
			case .Key( let key ):
				return "key: \(key)"

			case .Index( let index ):
				return "index: \(toString( index ))"
		}
	}

}
