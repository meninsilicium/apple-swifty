## Swift Utilities

General Swift utilities.

The code is under heavy work and subject to change.


#### Boolean
Boolean is what Bool should have been, an enum with some goodies.

#### Either
Either represents a value which can be of one type or another.

#### Optional extension
Added `<^>`, `<*>`, `fmap` and `>>*`

#### Pair
Pair encapsulates two values of a given type.

#### ResultOf
ResultOf represents a successful result with a value or a failure with an error.

#### curry / uncurry
Functions currying and uncurrying

#### hasValue / isEmpty
Functions to check for value existence or emptiness

#### operators
Many operators, experimental or not, exotic or not, and named functions.

### Links

- [Repository](https://github.org/meninsilicium/apple-swifty)

### Todo

At some point, I guess I'll turn this into a full module on its own.
