//
//  author:       fabrice truillot de chambrier
//  created:      22.01.2015
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


public class Generic0 {}

public class Generic1<T1> {}

public class Generic2<T1, T2> {}

public class Generic3<T1, T2, T3> {}

public class Generic4<T1, T2, T3, T4> {}

public class Generic5<T1, T2, T3, T4, T5> {}

public class Generic6<T1, T2, T3, T4, T5, T6> {}

public class Generic7<T1, T2, T3, T4, T5, T6, T7> {}

public class Generic8<T1, T2, T3, T4, T5, T6, T7, T8> {}

public class Generic9<T1, T2, T3, T4, T5, T6, T7, T8, T9> {}

public class Generic10<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> {}
