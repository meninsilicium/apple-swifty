//
//  author:       fabrice truillot de chambrier
//  created:      03.09.2014
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


#import <Foundation/Foundation.h>


@interface ExceptionsHelper : NSObject

+ (nullable NSException*) try: (nonnull void (^)(void)) block;
+ (void) try: (nonnull void (^)(void)) tryBlock catch: (nonnull void (^)(NSException* __nonnull)) catchBlock finally: (nonnull void (^)(void)) finallyBlock;

+ (void) throw: (nonnull NSException*) exception;
+ (void) throw: (nonnull NSString*) message reason: (nullable NSString*) reason;

@end
