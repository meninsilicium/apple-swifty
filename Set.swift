//
//  author:       fabrice truillot de chambrier
//  created:      12.02.2015
//
//  license:      see license.txt
//
//  © 2015-2015, men in silicium sàrl
//


import class Foundation.NSObject
import class Foundation.NSSet
import class Foundation.NSMutableSet


// MARK: Set

// Iterable

extension Set : Iterable_ {

	func foreach( @noescape function: (T) -> Void ) {
		for element in self {
			function( element )
		}
	}

	func foreach( @noescape function: (Int, T) -> Void ) {
		for (index, element) in Swift.enumerate( self ) {
			function( index, element )
		}
	}

}

// Enumerable

extension Set : Enumerable_ {

	func enumerate( @noescape function: (Int, ValueType) -> Bool ) {
		for (index, element) in Swift.enumerate( self ) {
			let next = function( index, element )
			if !next { break }
		}
	}

}

/// Set extension providing the usual monad functions

// MARK: Functor

extension Set : Functor_ {

	typealias ValueType = T
	typealias MappedType = HashableConstraint
	typealias MappedFunctorType = Set<MappedType>


	// functor map, <^>
	func map<R: Hashable>( @noescape transform: (T) -> R ) -> Set<R> {
		var set = Set<R>()
		for element in self {
			set.insert( transform( element ) )
		}

		return set
	}
	
}

// Pointed

extension Set : Pointed_ {

	static func pure( value: T ) -> Set<T> {
		return Set( CollectionOfOne( value ) )
	}

	init( _ value: T ) {
		self.init( CollectionOfOne( value ) )
	}

}

// Applicative

extension Set : Applicative_ {

	typealias ApplicativeType = [(T) -> MappedType]


	// applicative apply, <*>
	static func apply<R>( transform: [(T) -> R], value: Set<T> ) -> Set<R> {
		var set = Set<R>()
		var transformGenerator = transform.generate()
		var valueGenerator = value.generate()
		while true {
			if let transformElement = transformGenerator.next(), let valueElement = valueGenerator.next() {
				set.insert( transformElement( valueElement ) )
			}
			else {
				break
			}
		}

		return set
	}

}

// applicative apply, <*>
// using the default implementation

// Monad

extension Set : Monad_ {

	// monad bind, >>*
	func fmap<R>( @noescape transform: (T) -> Set<R> ) -> Set<R> {
		return Set<R>.flatten( self.map( transform ) )
	}

	// flatten
	static func flatten( value: Set<Set<T>> ) -> Set<T> {
		var set = Set<T>()
		for element in value {
			set.unionInPlace( element )
		}

		return set
	}

}

// monad bind, >>*
/*
public func >>*<T, R>( value: Set<T>, transform: (T) -> Set<R> ) -> Set<R> {
	return value.fmap( transform )
}

public func *<<<T, R>( transform: (T) -> Set<R>, value: Set<T> ) -> Set<R> {
	return value.fmap( transform )
}
*/

// MARK: HashableConstraint

// used to express the Hashable constraint of Set elements
class HashableConstraint : Hashable {

	var hashValue: Int {
		return 0
	}

}

func ==( lhs: HashableConstraint, rhs: HashableConstraint ) -> Bool {
	return true
}


// MARK: -
// MARK: NSSet

// Functor

extension NSSet : Functor {

	public func map( @noescape transform: (AnyObject) -> AnyObject ) -> NSSet {
		var set = NSMutableSet()
		for value in self {
			set.addObject( transform( value ) )
		}

		return set
	}

}

public func <^>( set: NSSet, @noescape transform: (AnyObject) -> AnyObject ) -> NSSet {
	return set.map( transform )
}

// Monad

extension NSSet : MonadSpecifics {

	typealias MappedType = AnyObject


	// monad bind, >>*
	public func fmap( @noescape transform: (AnyObject) -> NSSet ) -> NSSet {
		return NSSet.flatten( self.map( transform ) )
	}

	// flatten
	public static func flatten( value: NSSet ) -> NSSet {
		var set = NSMutableSet()
		for element in value {
			switch element {
				case let element as NSSet:
					set.unionSet( element as! Set<NSObject> )

				default:
					set.unionSet( element as! Set<NSObject> )
			}
		}

		return set
	}

}

public func >>*( value: NSSet, @noescape transform: (AnyObject) -> NSSet ) -> NSSet {
	return value.fmap( transform )
}

public func *<<( @noescape transform: (AnyObject) -> NSSet, value: NSSet ) -> NSSet {
	return value.fmap( transform )
}
