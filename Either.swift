//
//  author:       fabrice truillot de chambrier
//  created:      01.09.2014
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


/// Either encapsulate a value which can be of one type or another.
/// Right is the default value, because it's the right way to do so.

public enum Either<L, R> {

	// MARK: cases

	case Left( Box<L> )
	case Right( Box<R> )


	// MARK: initialization

	public init( _ value: R ) {
		self.init( right: value )
	}

	public init( left: L ) {
		self = .Left( Box( left ) )
	}

	public init( right: R ) {
		self = .Right( Box( right ) )
	}

	// MARK: -

	var isLeft: Bool {
		switch self {
			case .Left:
				return true

			case .Right:
				return false
		}
	}

	var left: L? {
		switch self {
			case .Left( let left ):
				return left.unbox()

			case .Right:
				return nil
		}
	}

	var isRight: Bool {
		switch self {
			case .Left:
				return false

			case .Right:
				return true
		}
	}

	func getLeft() -> L? {
		switch self {
			case .Left( let left ):
				return left.unbox()

			case .Right:
				return nil
		}
	}

	var right: R? {
		switch self {
			case .Left:
				return nil

			case .Right( let right ):
				return right.unbox()
		}
	}

	func getRight() -> R? {
		switch self {
			case .Left:
				return nil

			case .Right( let right ):
				return right.unbox()
		}
	}

}

extension Either : Functor {

	typealias MappedType = Any
	typealias MappedFunctorType = Either<L, MappedType>


	public func map<RL>( @noescape transform: (L) -> RL ) -> Either<RL, R> {
		switch self {
			case .Left( let left ):
				return Either<RL, R>( left: transform( left.unbox() ) )

			case .Right( let right ):
				return Either<RL, R>( right: right.unbox() )
		}
	}

	public func map<RR>( @noescape transform: (R) -> RR ) -> Either<L, RR> {
		switch self {
			case .Left( let left ):
				return Either<L, RR>( left: left.unbox() )

			case .Right( let right ):
				return Either<L, RR>( right: transform( right.unbox() ) )
		}
	}

}

public func <^><L, R, RL>( value: Either<L, R>, @noescape function: (L) -> RL ) -> Either<RL, R> {
	return value.map( function )
}

public func <^><L, R, RR>( value: Either<L, R>, @noescape function: (R) -> RR ) -> Either<L, RR> {
	return value.map( function )
}

extension Either : Pointed {

	public static func pure( value: R ) -> Either<L, R> {
		return Either( right: value )
	}

}

extension Either : Applicative {

	typealias ApplicativeType = Either<L, (R) -> MappedType>


	public static func apply<RR>( transform: Either<L, (R) -> RR>, value: Either<L, R> ) -> Either<L, RR> {
		switch (transform, value) {
			case (.Right( let transform ), .Right( let right )):
				return Either<L, RR>( right: transform.unbox()( right.unbox() ) )

			case (.Left( let left ), .Right):
				return Either<L, RR>( left: left.unbox() )

			case (.Right, .Left( let left )):
				return Either<L, RR>( left: left.unbox() )

			case (.Left( let left ), .Left):
				return Either<L, RR>( left: left.unbox() )
		}
	}

}

/*
func <*><L, R, RR>( transform: Either<L, (R) -> RR>, value: Either<L, R> ) -> Either<L, RR> {
	return Either.apply( transform, value: value )
}
*/

extension Either : Monad {

	public func fmap<RR>( @noescape transform: (R) -> Either<L, RR> ) -> Either<L, RR> {
		switch self {
			case .Left( let value ):
				return Either<L, RR>( left: value.unbox() )

			case .Right( let value ):
				return Either<L, RR>.flatten( self.map( transform ) )
		}
	}

	public static func flatten( value: Either<Either<L, R>, Either<L, R>> ) -> Either<L, R> {
		switch value {
			case .Left( let value ):
				return Either<L, R>( left: value.unbox().left! )

			case .Right( let value ):
				return Either<L, R>( right: value.unbox().right! )
		}
	}

	public static func flatten( value: Either<L, Either<L, R>> ) -> Either<L, R> {
		switch value {
			case .Left( let value ):
				return Either<L, R>( left: value.unbox() )

			case .Right( let value ):
				return Either<L, R>( right: value.unbox().right! )
		}
	}

}

// MARK: chaining

extension Either {

	public func onLeft( @noescape function: (L) -> Void ) -> Either {
		switch self {
			case .Left( let value ):
				function( value.unbox() )

			case .Right:
				break
		}

		return self
	}

	public func onRight( @noescape function: (R) -> Void ) -> Either {
		switch self {
			case .Left:
				break

			case .Right( let value ):
				function( value.unbox() )
		}

		return self
	}

}
