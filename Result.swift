//
//  author:       fabrice truillot de chambrier
//  created:      05.08.2014
//
//  license:      see license.txt
//
//  © 2014-2015, men in silicium sàrl
//


// MARK: public protocol Resultable

public protocol Resultable
{

	typealias ResultType

	var succeeded: Bool { get }
	var failed: Bool { get }

	var value: ResultType? { get }
	var error: Error? { get }

}


// MARK: -
// MARK: public enum Result

public enum Result : Resultable {

	typealias ResultType = Void

	// MARK: cases

	case Success
	case Failure( Error? )

}

// MARK: initialization

extension Result {

	public init() {
		self = .Success
	}

	public init( error: Error? ) {
		self = .Failure( error )
	}

	// MARK: convenience

	public var succeeded: Bool {
		switch self {
			case .Success:
				return true

			case .Failure:
				return false
		}
	}

	public var value: Void? {
		return nil
	}

	public var failed: Bool {
		switch self {
			case .Success:
				return false

			case .Failure:
				return true
		}
	}

	public var error: Error? {
		switch self {
			case .Success:
				return nil

			case .Failure( let error ):
				return error
		}
	}

	public func asa() -> Error? {
		return self.error
	}

}

// Iterable

extension Result : Iterable {

	typealias ValueType = Void


	public func foreach( @noescape function: () -> Void ) {
		switch self {
			case .Success:
				function()

			case .Failure( let error ):
				break
		}
	}

	public func foreach( @noescape function: (Int, Void) -> Void ) {
		switch self {
			case .Success:
				function( 0, () )

			case .Failure( let error ):
				break
		}
	}
	
}

// MARK: - public enum ResultOf<T>

/// ResultOf<T> encapsulate a successful result with a value or a failure with an error

public enum ResultOf<T> : Resultable {

	typealias ResultType = T


	// MARK: cases

	case Success( Box<T> )
	case Failure( Error? )

}

// MARK: initialization

extension ResultOf {

	public init( _ value: T ) {
		self = .Success( Box<T>( value ) )
	}

	public init( value: T ) {
		self = .Success( Box<T>( value ) )
	}

	public init( error: Error ) {
		self = .Failure( error )
	}

	public init( error: Error? ) {
		self = .Failure( error )
	}

	// MARK: convenience

	public var succeeded: Bool {
		switch self {
			case .Success:
				return true

			case .Failure:
				return false
		}
	}

	public var value: T? {
		switch self {
			case .Success( let box ):
				return box.unbox()

			case .Failure:
				return nil
		}
	}

	public func asa() -> T? {
		return self.value
	}

	public var failed: Bool {
		switch self {
			case .Success:
				return false

			case .Failure:
				return true
		}
	}

	public var error: Error? {
		switch self {
			case .Success:
				return nil

			case .Failure( let error ):
				return error
		}
	}

	public func asa() -> Error? {
		return self.error
	}

	public func tuple() -> ( T?, Error? ) {
		switch self {
			case .Success( let value ):
				return ( value.unbox(), nil )

			case .Failure( let error ):
				return ( nil, error )
		}
	}

}

// Iterable

extension ResultOf : Iterable {

	public func foreach( @noescape function: (T) -> Void ) {
		switch self {
			case .Success( let value ):
				function( value.unbox() )

			case .Failure:
				break
		}
	}

	public func foreach( @noescape function: (Int, T) -> Void ) {
		switch self {
			case .Success( let value ):
				function( 0, value.unbox() )

			case .Failure:
				break
		}
	}
	
}

public func ??<T>( result: ResultOf<T>, @autoclosure defaultValue: () -> T ) -> T {
	switch result {
	case .Success( let value ):
		return value.unbox() ?? defaultValue

	case .Failure:
		return defaultValue()
	}
}

// MARK: Functor

extension ResultOf : Functor {

	typealias MappedType = Any
	typealias MappedFunctorType = ResultOf<MappedType>


	public func map<R>( @noescape transform: (T) -> R ) -> ResultOf<R> {
		switch self {
			case .Success( let value ):
				return ResultOf<R>( value: transform( value.unbox() ) )

			case .Failure( let error ):
				return ResultOf<R>( error: error )
		}
	}

}

public func <^><T, R>( result: ResultOf<T>, @noescape transform: (T) -> R ) -> ResultOf<R> {
	return result.map( transform )
}

extension ResultOf : Pointed {

	public static func pure( value: T ) -> ResultOf<T> {
		return ResultOf<T>( value )
	}

}

// MARK: Applicative

extension ResultOf : Applicative {

	typealias ApplicativeType = ResultOf<(T) -> MappedType>


	public static func apply<R>( transform: ResultOf<(T) -> R>, value: ResultOf<T> ) -> ResultOf<R> {
		switch (transform, value) {
			case (.Success( let transform ), .Success( let value )):
				let r = transform.unbox()( value.unbox() )
				return ResultOf<R>( value: r )

			case (.Failure( let error ), .Success):
				return ResultOf<R>( error: error )

			case (.Success, .Failure( let error )):
				return ResultOf<R>( error: error )

			case (.Failure( let error ), .Failure):
				return ResultOf<R>( error: error )
		}
	}

}

/*
func <*><T, R>( transform: ResultOf<(T) -> R>, value: ResultOf<T> ) -> ResultOf<R> {
	return ResultOf.apply( transform, value: value )
}
*/

// MARK: Monad

extension ResultOf : Monad {

	public func fmap<R>( @noescape transform: (T) -> ResultOf<R> ) -> ResultOf<R> {
		switch self {
			case .Success( let value ):
				return ResultOf<R>.flatten( self.map( transform ) )

			case .Failure( let error ):
				return ResultOf<R>( error: error )
		}
	}

	public static func flatten( value: ResultOf<ResultOf<T>> ) -> ResultOf<T> {
		switch value {
			case .Success( let value ):
				return value.unbox()

			case .Failure( let error ):
				return ResultOf<T>( error: error )
		}
	}

}

/*
public func >>*<T, R>( value: ResultOf<T>, transform: (T) -> ResultOf<R> ) -> ResultOf<R> {
	return value.fmap( transform )
}

public func *<<<T, R>( transform: (T) -> ResultOf<R>, value: ResultOf<T> ) -> ResultOf<R> {
	return value.fmap( transform )
}
*/

// MARK: chaining

extension ResultOf {

	public func onSuccess( @noescape function: (T) -> Void ) -> ResultOf {
		switch self {
			case .Success( let value ):
				function( value.unbox() )

			case .Failure:
				break
		}

		return self
	}

	public func onFailure( @noescape function: (Error?) -> Void ) -> ResultOf {
		switch self {
			case .Success:
				break

			case .Failure( let error ):
				function( error )
		}

		return self
	}

}
